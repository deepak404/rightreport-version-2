<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchemeDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheme_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('scheme_code');
            $table->string('scheme_name');
            $table->string('scheme_type');
            $table->string('scheme_sub');
            $table->string('amc_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scheme_details');
    }
}
