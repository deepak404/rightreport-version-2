<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraw_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('investor_id');
            $table->string('investor_type');
            $table->string('scheme_name');
            $table->string('scheme_code');
            $table->double('withdraw_amount');
            $table->string('scheme_type');
            $table->date('withdraw_date');
            $table->double('purchase_nav');
            $table->double('current_nav');
            $table->double('units');
            $table->string('folio_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('withdraw_details');
    }
}
