<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('investor_id');
            $table->string('investor_type');
            $table->string('scheme_name');
            $table->string('scheme_code');
            $table->double('amount_inv');
            $table->string('scheme_type');
            $table->date('purchase_date');
            $table->double('purchase_nav');
            $table->double('units');
            $table->string('folio_number');
            $table->boolean('is_inv');
            $table->boolean('is_notseen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('investment_details');
    }
}
