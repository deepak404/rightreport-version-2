<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDividendsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dividends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('investment_id');
            $table->integer('investor_id');
            $table->string('investor_type');
            $table->string('scheme_name');
            $table->double('investment_amount');
            $table->date('investment_date');
            $table->date('divident_date');
            $table->double('divident_amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dividends');
    }
}
