<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

  <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/login-responsive.css">
  <link rel="stylesheet" href="css/jquery-ui.css">
  <link rel="stylesheet" href="css/jqueryui.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
  <script src="js/index.js?v=1.0"></script>
  <script src="js/loader.js"></script>

</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
  <div class="container-fluid" id="navbar_container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
      <a class="navbar-brand" href="#"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
        @if(\Auth::user()->name == 'Rightfunds')
            <a href="/bonds" class="module-links">Bonds</a>
            <a href="/pms" class="module-links">PMS</a>
        @endif
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <div class="col-xs-3 "><p class="img-circle" id = "profile_text">V</p></div>
        <div class="col-xs-5 padding-lr-zero">
          <span id = "user_name">{{\Auth::user()->name}}</span>
        </div>
        <div class="col-xs-2 text-center padding-lr-zero">
          <div class="dropdown" id="drop">
                <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                <ul class="dropdown-menu">
                  <li><a href="#" id="settings">Setting</a></li>
                  <li><a href="{{url('/logout')}}" id="logout">Logout</a></li>
                </ul>
            </div>
        </div>
      </li>
    </ul>
  </div>
</nav>


<div class = "container-fluid">

<div class="row">
  <div class="col-lg-3 col-md-3" id="sidebar_wrapper">
    <!--<div class="row">-->
      <div id="sidebar">
        <div class="col-xs-12" id="op_bar">
          <div class="col-xs-6 text-center"><a href="#" class="text-center add_group"><i class="material-icons top_bar_icons">group_add</i></a></div>
          <div class="col-xs-6 text-center"><a href="#" id = "add_person" class="text-center add_person"><i class="material-icons top_bar_icons">person_add</i></a></div>

          {{--<div class="col-xs-3 text-center"><a href="#" id = "set_date" class="text-center "><i class="material-icons top_bar_icons">today</i></a></div>--}}

          {{--<div class="col-xs-3 text-center"><a href="#" class="text-center"><i class="material-icons top_bar_icons">search</i></a></div>--}}
        </div>
        <div class="col-xs-12" id="search_bar">
          <input type="text" id="search-input" placeholder="Search">
          <i class="material-icons" id="search-icon">search</i>
        </div>


        <div id="client_wrapper" class="client_wrapper col-xs-12">
          
          @foreach($groups as $group)
            <!-- Group Head starts -->
             <div class="col-xs-12 padding-lr-zero client_bar">
                  <div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="#" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><a data-id = "{{$group->id}}" data-groupname="{{$group->id}}" class="client_name mont-reg group">{{$group->name}}</a></div></div>
                  <div class="col-xs-4 text-center">
                      <i class="material-icons side_icon dot_btn dropdown-toggle" data-toggle = "dropdown" >more_horiz</i>
                          <ul class="dropdown-menu sub_person_option">
                            <li><a href="#" class="add_group_person" data-id = "{{$group->id}}">Add Person</a></li>
                            <li><a href="#" class="group_rename" data-id = "{{$group->id}}">Rename</a></li>
                            <li><a href="#" id="logout" class="remove" data-id = "{{$group->id}}">Remove</a></li>
                          </ul>

                      <span><a href="#{{$group->id}}" data-toggle = "collapse" class="side_icon_parent"><i class="material-icons side_icon  key_right">keyboard_arrow_right</i></a></span>                  
                  </div>
              </div>

            <!-- Group Head ends -->

            <!-- group members starts -->

              <div class="col-xs-12 padding-lr-zero collapse" id="{{$group->id}}">

                        <!--<div class="sub_person_bar col-xs-12">
                          <div class="col-xs-10">                   
                              <a href="" class="sub-menu sub-person">Naveen</a>
                          </div>
                          <div class="col-xs-2">
                            <span><a href="" class=""><i class="material-icons side_icon dot_btn">more_horiz</i></a></span>
                          </div>
                        </div>-->

                        @foreach($group_members as $members)
                          @if($members->group_id == $group->id)
                            <div class="sub_person_bar col-xs-12">
                                    <div class="col-xs-8">                  
                                        <a href="#" class="sub-menu sub-person" data-groupid = "{{$group->id}}" data-subid = "{{$members->id}}" data-subperson = "{{$members->id}}">{{$members->member_name}}</a>
                                    </div>
                                    <div class="col-xs-2 padding-lr-zero">
<!--                                       @if(\Auth::user()->email == 'admin@rightfunds.com')
                                      <i class="material-icons noti-icon">chat_bubble</i>
                                      @endif
                                      @foreach($queries as $query)
                                      @if($members->id == $query->investor_id)
                                        <i class="material-icons query-icon">info</i>
                                      @endif
                                      
                                      @endforeach -->
                                     <!--  <a href="/person/{{$members->id}}" target="_blank"><i class="material-icons">pie_chart</i></a>
                                      <a href="/bmGroupmember/{{$members->id}}" target="_blank"><i class="material-icons">equalizer</i></a> -->
                                    </div>
                                    <div class="col-xs-2">
                                      <div class="dropdown" id="drop">
                                        <i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>
                                            <ul class="dropdown-menu sub_person_option">
                                                <li><a href="#" id="add-sip" class="add_sip" data-type="subperson" data-id = "{{$members->id}}">Add SIP</a></li>
                                                <li><a href="/show_sip/subperson/{{$members->id}}" target="_blank">Show SIP</a></li>
                                                <li><a href="#" id="rename" class="sub_person_rename" data-id = "{{$members->id}}">Rename</a></li>
                                                <li><a href="#" id="remove" class="sub_person_remove" data-id = "{{$members->id}}">Remove</a></li>
                                            </ul>
                                        </div> 
                                    </div>
                            </div>
                          @endif
                        @endforeach

                        
                  <div class="sub_person_bar col-xs-12">
                            <div class="col-xs-8 cis_div">                  
                                <a href="#" class="cis" data-groupid = "{{$group->id}}">CIS</a>
                            </div>
      <!--                       <div class="col-xs-2 padding-lr-zero">
                              <a href="/id/{{$group->id}}" target="_blank" class="portfolioGroup" data-groupid = "{{$group->id}}"> <i class="material-icons">pie_chart</i></a>
                            </div>
                            <div class="col-xs-2">
                              <a href="/bmCIS/{{$group->id}}" target="_blank"><i class="material-icons">equalizer</i></a>
                            </div> -->
                  </div>
      
                  <!-- <div class="sub_person_bar col-xs-12">
                      <div class="col-xs-8 padding-l-zero"> 
                        <div class="col-xs-2 padding-l-zero">
                          <i class="material-icons">pie_chart</i>
                        </div>                  
                        <div class="col-xs-10 padding-l-zero">
                          <a href="/id/{{$group->id}}" target="_blank" class="portfolioGroup" data-groupid = "{{$group->id}}">Portfolio</a>
                        </div>
                      </div>
                  </div> -->
                          

             </div>
             <!-- group members ends -->
        @endforeach

        @foreach($persons as $person)

                        <div class="sub_person_bar col-xs-12">
                        
                          <div class="col-xs-8 padding-l-zero"> 
                            <div class="col-xs-2 padding-l-zero">
                              <i class="material-icons">person</i>
                            </div>                  
                            <div class="col-xs-10 padding-l-zero">
                              <a href="#" data-id = "{{$person->id}}" class="sub-menu individual ind_per" data-person = "{{$person->id}}">{{$person->name}}</a>
                            </div>
                          </div>
                          <div class="col-xs-2 padding-lr-zero">
<!--                             @if(\Auth::user()->email == 'admin@rightfunds.com')
                              <i class="material-icons ind-noti-icon">chat_bubble</i>
                            @endif
                            @foreach($queries as $query)
                            @if($person->id == $query->investor_id)
                              <i class="material-icons query-icon">info</i>
                            @endif
                            
                            @endforeach --><!-- 
                              <a href="/person/{{$person->id}}" target="_blank"><i class="material-icons">pie_chart</i></a>
                              <a href="/bmSingle/{{$person->id}}" target="_blank"><i class="material-icons">equalizer</i></a> -->
                          </div>
                          <div class="col-xs-2">

                              <div class="dropdown" id="drop">
                              <i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>
                                  <ul class="dropdown-menu sub_person_option">
                                      <li><a href="#" id="add-sip" class="add_sip" data-type="person" data-id = "{{$person->id}}">Add SIP</a></li>
                                      <li><a href="/show_sip/person/{{$person->id}}" target="_blank">Show SIP</a></li>
                                      <li><a href="#" id="rename" class="person_rename" data-id = "{{$person->id}}">Rename</a></li>
                                    <li><a href="#" id="remove" class="person_remove" data-id = "{{$person->id}}">Remove</a></li>
                                  </ul>
                              </div>                   
                          </div>
                        </div>

        @endforeach
            @if(\Auth::user()->name == 'Rightfunds')
              <div class="sub_person_bar col-xs-12">

                <div class="col-xs-8 padding-l-zero">
                  <div class="col-xs-2 padding-l-zero">
                    <i class="material-icons">receipt</i>
                  </div>
                  <div class="col-xs-10 padding-l-zero">
                    <a href="#" class="brokerage">Brokerage</a>
                  </div>
                </div>
                <div class="col-xs-2 padding-lr-zero">
                  <!--                                                          --><!--
                              <a href="/person/4" target="_blank"><i class="material-icons">pie_chart</i></a>
                              <a href="/bmSingle/4" target="_blank"><i class="material-icons">equalizer</i></a> -->
                </div>
              </div>
              @endif
        </div>


      </div>
    <!--</div>-->
  </div>
  <div class="col-lg-9 col-md-9" id="contentbar_wrapper">
    <div id="contentbar">
    <div class="col-lg-12 col-md-12 " id="client_relbar">
      <p id="client_det"><span id="client_parent">Group</span><span><img src="icons/arrow.png" id="arrow_img" /></span><span id="client_child">Member</span></p>


      <div id="scheme_export">
        @if(\Auth::user()->edit_access==1)
          {{--<form target="_blank" id="sip_form" action="/show_sip_list" method="POST" style="display: inline;">--}}
            {{--{{csrf_field()}}--}}
            {{--<input type="hidden" name="investor_id" value="" id="ex_investor_id" />--}}
            {{--<input type="hidden" name="investor_type" value="" id="ex_investor_type" />--}}
            {{--<button type="submit" class="btn btn-primary" id="export_btn">SIP List</button>--}}
          {{--</form>--}}

          <button type="button" class="btn btn-primary" id="add_scheme_btn">Add Scheme</button>
        @endif
        <form target="_blank" id="export_form" action="/download_investments" method="POST" style="display: inline;">
          {{csrf_field()}}
          <input type="hidden" name="ex_nav_date" value="" id="ex_nav_date" />
          <input type="hidden" name="investor_id" value="" id="ex_investor_id" />
          <input type="hidden" name="investor_type" value="" id="ex_investor_type" />
          <button type="submit" class="btn btn-primary" id="export_btn">Export</button>
        </form>
    </div>
      <span id="all-container">
        <input type="text" name="pan_no" id = "enter_pan" class="pan_no" data-invtype = "" data-invid = "" style="display: none;" placeholder="Enter PAN No" maxlength="10" minlength="10">
        <span id="pan_container" style="display: none;">
            <span id = "pan_no_text">PAN No - </span><span id="user_pan_no">BFRPN4910B</span>
              <span id="edit_container">
                <i class="material-icons" data-invtype = "" data-invid = "" id="edit_pan" data-toggle = "tooltip" title="Edit PAN">mode_edit</i>
              </span>
              <span id="nav_as">NAV as of <span id="show_current_date" style="color: #00AF64;"> 11-07-2017</span><!--<input type="text" name="nav_date" id="nav_date" data-invid = "" data-invtype = "">--></span>
              <span id="xirr"></span>
        </span>
      </span>
    
    </div>


      <div class="col-lg-12 col-md-12" id="invdetails_bar">
        <div class="col-lg-2 col-md-2"><p class="content_header">Current Amount Invested</p><p class="amount_header">Rs. <span id="total_amount_inv">-</span></p></div>
        <div class="col-lg-2 col-md-2"><p class="content_header">Current Market Value</p><p class="amount_header">Rs. <span id="current_market_value">-</span></p></div>
        <div class="col-lg-2 col-md-2"><p class="content_header">Unrealised Profit/loss</p><p class="amount_header">Rs. <span id="profit_or_loss">-</span></p></div>
        <div class="col-lg-2 col-md-2"><p class="content_header">Net Amount Invested</p><p class="amount_header">Rs. <span id="net_amount_inv">-</span></p></div>
        <div class="col-lg-2 col-md-2"><p class="content_header">Realised Profit/Loss</p><p class="amount_header">Rs. <span id="real_p_or_l">-</span></p></div>


        {{--<div class="col-lg-3 col-md-3"><p class="content_header">Absolute Returns</p><p class="amount_header"><span id="absolute_ret_avg">-</span>%</p></div>--}}

      </div>

    <div class="col-lg-12 col-md-12 padding-lr-zero">
      <div class="table-wrapper" id="table-wrapper">
        <h4 class="table-name-header purchase-header">Mutual Fund Investments</h4>
        <table class="table table-bordered" id="investment_table">
        <thead id="inv_table_head">
          <tr>
            <th><p class="table_heading">Scheme Name</p></th>
            <th><p class="table_heading">Scheme Type</p></th>
            <th><p class="table_heading">Date of Purchase</p></th>
            <th><p class="table_heading">Amt. Invested</p></th>
            <th><p class="table_heading">Purchase NAV</p></th>
            <th><p class="table_heading">Units</p></th>
            <th><p class="table_heading">Current Nav.</p></th>
            <th><p class="table_heading">Current Mrkt Val.</p></th>
            <th><p class="table_heading">Unrealised Profit/Loss</p></th>
            <th><p class="table_heading">Absolute Returns(%)</p></th>
            <th><p class="table_heading">Annualised Returns</p></th>
            <th><p class="table_heading">Folio Number</p></th>
              <th><p class="table_heading">Trail Fee(%)</p></th>
              <th><p class="table_heading">Upfront Fee(%)</p></th>
              <th><p class="table_heading">Last Updated</p></th>
          </tr>
        </thead>
        <tbody id="investment_table_body">


        </tbody>
      </table>


          <h4 class="table-name-header purchase-header" style="background-color:#036cf44d">Systematic Investment Plan</h4>
          <table class="table table-bordered" id="sip_table">
              <thead id="inv_table_head">
              <tr>
                  <th><p class="table_heading">Scheme Name</p></th>
                  <th><p class="table_heading">Scheme Type</p></th>
                  <th><p class="table_heading">Date of Purchase</p></th>
                  <th><p class="table_heading">Amt. Invested</p></th>
                  <th><p class="table_heading">Purchase NAV</p></th>
                  <th><p class="table_heading">Units</p></th>
                  <th><p class="table_heading">Current Nav.</p></th>
                  <th><p class="table_heading">Current Mrkt Val.</p></th>
                  <th><p class="table_heading">Unrealised Profit/Loss</p></th>
                  <th><p class="table_heading">Absolute Returns(%)</p></th>
                  <th><p class="table_heading">Annualised Returns</p></th>
                  <th><p class="table_heading">Folio Number</p></th>
                  <th><p class="table_heading">Trail Fee(%)</p></th>
                  <th><p class="table_heading">Upfront Fee(%)</p></th>
                  <th><p class="table_heading">Last Updated</p></th>
              </tr>
              </thead>
              <tbody id="sip_table_body">


              </tbody>
          </table>


          <h4 class="table-name-header purchase-header" style="background-color: #00bcd480">Liquid Investments</h4>
          <table class="table table-bordered" id="liquid_table">
              <thead id="inv_table_head">
              <tr>
                  <th><p class="table_heading">Scheme Name</p></th>
                  <th><p class="table_heading">Scheme Type</p></th>
                  <th><p class="table_heading">Date of Purchase</p></th>
                  <th><p class="table_heading">Amt. Invested</p></th>
                  <th><p class="table_heading">Purchase NAV</p></th>
                  <th><p class="table_heading">Units</p></th>
                  <th><p class="table_heading">Current Nav.</p></th>
                  <th><p class="table_heading">Current Mrkt Val.</p></th>
                  <th><p class="table_heading">Unrealised Profit/Loss</p></th>
                  <th><p class="table_heading">Absolute Returns(%)</p></th>
                  <th><p class="table_heading">Annualised Returns</p></th>
                  <th><p class="table_heading">Folio Number</p></th>
                  <th><p class="table_heading">Trail Fee(%)</p></th>
                  <th><p class="table_heading">Upfront Fee(%)</p></th>
                  <th><p class="table_heading">Last Updated</p></th>
              </tr>
              </thead>
              <tbody id="liquid_table_body">


              </tbody>
          </table>


        <h4 class="table-name-header redeem-header">Redemption Detail</h4>
        <table class="table table-bordered" id="withdraw_table">
          <thead id="wd_table_head">
          <tr>
            <th><p class="table_heading">Scheme Name</p></th>
            <th><p class="table_heading">Folio Number</p></th>
            <th><p class="table_heading">Date of Purchase</p></th>
            <th><p class="table_heading">Date of Withdraw</p></th>
            <th><p class="table_heading">Amt. Invested</p></th>
            <th><p class="table_heading">Purchase NAV</p></th>
            <th><p class="table_heading">Units</p></th>
            <th><p class="table_heading">Redemption Nav.</p></th>
            <th><p class="table_heading">Withdraw Mrkt Val.</p></th>
            <th><p class="table_heading">Realised Profit/Loss</p></th>
            <th><p class="table_heading">Exit Load</p></th>
            <th><p class="table_heading">STT</p></th>
            {{--<th><p class="table_heading">Absolute Returns(%)</p></th>--}}

          </tr>
          </thead>
          <tbody id="withdraw_table_body">

          </tbody>
        </table>
        
        <h4 class="table-name-header dividend-header">Dividend Detail</h4>
        <table class="table table-bordered" id="dividend_table">
          <thead id="div_thead">
            <tr>
              <th><p class="table_heading">Scheme Name</p></th>
              <th><p class="table_heading">Date of Purchase</p></th>
              <th><p class="table_heading">Amt. Inveated</p></th>
              <th><p class="table_heading">Dividend Date</p></th>
              <th><p class="table_heading">Dividend per Unit</p></th>
              <th><p class="table_heading">Divident Amount</p></th>
            </tr>
          </thead>
          <tbody id="dividend_tbody">
            
          </tbody>
        </table>

      </div>
    </div>

    </div>
  </div>
</div>


</div>


<!-- Modals Begin below -->

<div id="addSipModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add SIP</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_sip_form">
                    <div class="form-group">
                        <select class="mont-reg" name="scheme_name" id = "scheme_name" required>
                            @foreach($schemes as $scheme)
                                <option value = "{{$scheme->scheme_code}}">{{$scheme->scheme_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="mont-reg input-field" id="sip_amount" placeholder="SIP Amount" name="sip_amount">
                    </div>

                    <input type="hidden" id="sip_investor_id" name="sip_investor_id">
                    <input type="hidden" id="sip_investor_type" name="sip_investor_type">

                    <div class="form-group">
                        <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Add">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>

<div id="addSchemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Add Scheme</h4>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="add_scheme_form">
          <div class="form-group">
            <select class="mont-reg" name="scheme_name" id = "scheme_name" required>

              @foreach($schemes as $scheme)
                <option value = "{{$scheme->scheme_code}}">{{$scheme->scheme_name}}</option>
              @endforeach
                
            </select>

          </div>
          <div class="form-group">
            <select class="mont-reg" name="inv_types" id="inv_type">
                <option value = "1">One Time</option>
                <option value = "2">SIP</option>
                <option value = "3">Liquid</option>
            </select>
          </div>
          <div class="form-group">
            <select class="mont-reg" name="option_type" id="option_type">
                <option value = "Growth">Growth</option>
                <option value = "Monthly Dividend">Monthly Dividend</option>
            </select>
          </div>
          <input type="hidden" name="investor_id" id="investor_id" value="">
          <input type="hidden" name="investor_type" id="investor_type" value="">

          <div class="form-group">
            <input type="text" class="mont-reg" name="dop" id = "dop" required placeholder="Enter Date of Purchase">
          </div>

          <div class="form-group">
            <input type="number" class="mont-reg" name="amt_inv" id = "amt_inv" required placeholder="Amount Invested">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="purchase_nav" id = "purchase_nav" required placeholder="Purchase Nav">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="folio_number" id = "folio_number" required placeholder="Folio Number">
          </div>


            <div class="form-group">
                <input type="text" class="mont-reg input-field" name="trail_fee" id = "trail_fee" required placeholder="Trail Fee %">
            </div>

            <div class="form-group">
                <input type="text" class="mont-reg input-field" name="upfront_fee" id = "upfront_fee" required placeholder="Upfront Fee %">
            </div>

          <div class="form-group">
            <input type="checkbox" class="mont-reg" name="is_inv" id = "is_inv">
            <span class="mont-reg">Don't inclued as investment</span>
          </div>

          <div class="form-group">
            <input type="checkbox" class="mont-reg" name="is_aum" id = "is_aum">
            <span class="mont-reg">Don't inclued to AUM</span>
          </div>


          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Add">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>

<div id="addUserModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title" id="modal_header2">Add User</h4>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="add_user_form">
          <input type="hidden" name="investor_type" id="investor_type2" value="">
          <input type="hidden" name="group_id2" id="group_id2" value="">
          <input type="hidden" name="type" id="type" value="">


          <div class="form-group">
            <input type="text" class="mont-reg" name="inv_name" id = "inv_name" required placeholder="Enter Investor Name">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="pan" id = "pan" required placeholder="PAN Number" style="text-transform:uppercase">
          </div>

          <div class="form-group">
            <input type="email" class="mont-reg" name="email_id" id = "email_id" required placeholder="Email Id">
          </div>

          <div class="form-group">
            <input type="number" class="mont-reg" name="mobile_number" id = "content_number" required placeholder="Mobile Number">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="address" id = "address" required placeholder="Address of Investor">
          </div>

          <div class="form-group">
            <input type="submit" name="add_user_btn" id = "add_user" class="btn btn-primary blue-btn" value="Add">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>


<div id="settingsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Settings</h4>
        <p class="text-center" id="change_pass">Change Password</p>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="change_password_form">
          <div class="form-group">
            <input type="text" class="mont-reg" name="old_password" id = "old_password" required placeholder="Old Password">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="new_password" id = "new_password" required placeholder="New password">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="repeat_password" id = "repeat_password" required placeholder="Repeat New Password">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Save">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>



<div id="adminPasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Admin Password</h4>
        <p class="text-center" id="admin_text">Admin Password is required to delete this investment.</p>
      </div>
      <div class="modal-body">
        
          <div class="form-group">
            <input type="password" class="mont-reg" name="admin_password" id = "admin_password" required placeholder="Enter Admin Password">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "check_admin_pass" class="btn btn-primary blue-btn" value="Delete">
          </div>

        
      </div>
    </div>

  </div>
</div>


<div id="userStatusModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Status</h4>
      </div>
      <div class="modal-body">
        <p id="addition_status" class="mont-reg text-center"></p>
      </div>
    </div>

  </div>
</div>

<div id="setDateModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Set Date</h4>
      </div>
      <div class="modal-body">
        <input type="text" name="nav_date" id="nav_date" class="mont-reg text-center">
      </div>
    </div>

  </div>
</div>


<div id="getQueryModal" class="modal fade" role="dialog">
  <div class="modal-dialog query-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <span class="modal-title" id="query_holder"></span>
        <div style="display: inline;" class="pull-right">
          <button type="button" id="add_query" data-invid = "" data-invtype=""  class="btn btn-primary">Add</button>
          <button type="button" class="close"  data-dismiss="modal">&times;</button>
        </div>
      </div>
      <div class="modal-body">
        <table class="table table-condensed">
    <thead>
      <tr>
        <th>Query</th>
        <th>Updation Date</th>
        <th>Status</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
    <tbody id="query_body">
      <!--<tr>
        <td>Mandate Issue</td>
        <td><input type="text" name="query_date" class="query_date"></td>
        <td><select id="query_select"><option>InProcess</option><option>Complete</option></select></td>
        <td><i class="material-icons">delete</i></td>
        <td><input type="button" name="save_query" id="save_query" value="save"  class="btn btn-primary"></td>
      </tr>
      <tr>
        <td>Mandate Issue</td>
        <td>22/17/2017</td>
        <td><select id="query_select"><option>InProcess</option><option>Complete</option></select></td>
        <td><i class="material-icons">delete</i></td>
        <td><input type="button" name="save_query" id="save_query" value="save" class="btn btn-primary"></td>
      </tr>
      <tr>
        <td>Mandate Issue</td>
        <td>22/17/2017</td>
        <td><select id="query_select"><option>InProcess</option><option>Complete</option></select></td>
        <td><i class="material-icons">delete</i></td>
        <td><input type="button" name="save_query" id="save_query" value="save" class="btn btn-primary"></td>
      </tr>-->
    </tbody>
  </table>
      </div>
    </div>

  </div>
</div>


<!-- Model for edit investment type -->
<div id="edit_model" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">AUM Master Control</h4>
      </div>
      <div class="modal-body">
       <form id="update_aum">
          <input type="hidden" name="inv_id" id="ivest_id" value="">
          <div class="form-group">
            <select class="mont-reg" name="aum_state" id = "edit_name" required>
                  <option value = "0">Add to AUM</option>
                  <option value = "1">Don't add to AUM</option>
            </select>
          </div>
          <div class="form-group">
            <input type="submit" name="edit_inv_btn" id = "edit_inv" class="btn btn-primary blue-btn" value="Update">
          </div>
       </form>
      </div>
    </div>
  </div>
</div>


<div id="dividendModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Dividend</h4>
      </div>
      <div class="modal-body">
        <form name="dividend-form" id="dividend-form">
          <input type="hidden" name="inv-id" id="invs-id">
          <input type="text" name="dividend-units" id="dividend-units" placeholder="Dividend Per Units" class="mont-reg text-center" required>
          <input type="text" name="dividend-date" id="dividend-date" class="mont-reg text-center" placeholder="Payout Date" required>
          <input type="text" name="dividend-amount" id="dividend-amount" class="mont-reg text-center" placeholder="Dividend Amount" required>
          <input type="submit" name="submit" id="dividend-btn" value="dividend" class="center-block btn btn-primary">
        </form>
      </div>
    </div>

  </div>
</div>


<div id="redeemModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Redeem</h4>
      </div>
      <div class="modal-body">
        <form name="redeem-form" id="redeem-form">
          <input type="hidden" name="inv-id" id="inv-id">
          <input type="text" name="redeem-units" id="redeem-units" placeholder="Redeem Units" class="mont-reg text-center" required>
          <input type="text" name="redeem-date" id="redeem-date" class="mont-reg text-center" placeholder="Redeem Date" required>
          <input type="text" name="stt" id="stt" class="mont-reg text-center" placeholder="STT" required>
          <input type="text" name="exit-load" id="exit-load" class="mont-reg text-center" placeholder="Exit Load" required>
          {{--<input type="text" name="price-nav" id="price-nav" class="mont-reg text-center" placeholder="Price Nav" required>--}}
           <div class="text-center" id="all-units-holder">
             <label for="all-units">All Units Redemtion</label>
             <select name="all-units" id="all-units">
               <option value="no">No</option>
               <option value="yes">Yes</option>
             </select>
           </div>
          <input type="submit" name="submit" value="Redeem" class="center-block btn btn-primary">
        </form>
      </div>
    </div>

  </div>
</div>






<div id="brokerageModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Brokerage</h4>
      </div>
      <div class="modal-body">
        <form name="brokerage-form" target="_blank" method = "POST" action="/download_brokerage" id="brokerage-form">
          {{csrf_field()}}
          <div class="text-center">
            <select name="month" id="month">
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
            <input type="text" id="year" placeholder="Year" class="input-field" name="year" required>
          </div>
          <input type="submit" name="submit" id="brok-btn" value="Submit" class="center-block btn btn-primary">
        </form>
      </div>
    </div>

  </div>
</div>



<div id="editInvestmentModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Edit Investment</h4>
            </div>
            <div class="modal-body">
                <form name="edit-investment-form" method = "POST" id="edit-investment-form">
                    {{csrf_field()}}
                    <div class="text-center">
                        <input type="hidden" name="investment-id" value="" id="inv-id">
                        <input type="text" name="trail-fee" placeholder="Trail Fee %" class="input-field" id="trail-fee">
                        <input type="text" name="upfront-fee" placeholder="Upfront Fee %" class="input-field" id="upfront-fee">
                    </div>
                    <input type="submit" name="submit" value="Submit" class="center-block btn btn-primary modal-submit-btn">
                </form>
            </div>
        </div>

    </div>
</div>



<div id="accStatementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Account Statement Upload</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="account_statement_form">

                    <div class="form-group">
                        <input type="file" class="mont-reg center-block" name="acc-statement" id = "acc-statement" required>
                        <input type="hidden" name="inv-id" id="inv-id">
                        <input type="hidden" name="upload-type" id="upload-type">
                        <input type="hidden" name="u-type" id="u-type">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary center-block modal-submit-btn" value="Upload">
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">



</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.1.1/typeahead.bundle.js"></script>
<script src="js/smart_search.js"></script>


</body>
</html>

