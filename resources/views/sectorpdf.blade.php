<!DOCTYPE html>
<html>
<head>
	<title>SECTOR PDF</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
  	<link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/sectorpdf.css">
</head>
<body>
  	<div id="header-view">
  		<img class="logo" src = "icons/rfib_logo.png"/>
  		<div id="address-div">
  			<p class="m-b-0">Level 1, No 1, Balaji First Avenue,</p> 
  			<p class="m-b-0">T.Nagar,</p> 
  			<p class="m-b-0">Chennai - 600017</p>
  			<p class="m-b-0">Ph: +91 8825888200</p>
  		</div>
  	</div>
    <p>Overall Portfolio</p>
	<table class="table table-bordered" id="sector-table">
          <thead class="table-head">
            <tr>
              <th>Sector Name</th>
              <th>Amount Invested</th>
              <th>Percentage</th> 
            </tr>
          </thead>
          <tbody>
            @foreach($sector_array as $val)
            <tr>
              <td>{{$val[0]}}</td>
              <td>{{$val[1]}}</td>
              <td>{{$val[2]}}%</td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
            <tr>
              <th>Grand Total</th>
              <th>{{$total}}</th>
              <th>100%</th>  
            </tr>
          </tfoot>
          </thead>
      </table>
      <div class="table-chart">
      <div class="category-table">
      <table class="table table-bordered" id="category-table">
          <thead>
            <tr>
              <th>Category Name</th>
              <th>Amount Invested</th>
              <th>Percentage</th>
            </tr>
          </thead>  
          <tbody>
            <tr>
              <td>Equity</td>
              <td>{{$equity}}</td>
              <td>{{$equity_avg}}</td>
            </tr>
            <tr>
              <td>Debt</td>
              <td>{{$debt}}</td>
              <td>{{$debt_avg}}</td>
            </tr>
            <tr>
              <td>Balanced</td>
              <td>{{$balanced}}</td>
              <td>{{$balanced_avg}}</td>
            </tr>
          </tbody>
          <tfoot>
           <tr>
              <th>Grand Total</th>
              <th>{{$total}}</th>
              <th>100%</th>
            </tr>
          </tfoot>
      </table>
    </div>
      <div class="sector-chart-container">
          <canvas id="sector-pie-chart">
            <img id="url" />
          </canvas>
     </div>
   </div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    var temp = [];
    var ctx1 = document.getElementById("sector-pie-chart").getContext('2d');
    var data1 = {

       
        labels: ["{{$sector_array[0][0]}} ({{$sector_array[0][2]}}%)", "{{$sector_array[1][0]}} ({{$sector_array[1][2]}}%)", "{{$sector_array[2][0]}} ({{$sector_array[2][2]}}%)", "{{$sector_array[3][0]}} ({{$sector_array[3][2]}}%)", "{{$sector_array[4][0]}} ({{$sector_array[4][2]}}%)","{{$sector_array[5][0]}} ({{$sector_array[5][2]}}%)","{{$sector_array[7][0]}} ({{$sector_array[7][2]}}%)","{{$sector_array[8][0]}} ({{$sector_array[8][2]}}%)","{{$sector_array[6][0]}} ({{$sector_array[6][2]}}%)"],
        datasets: [
            {
                label: "TeamA Score",
                data: ["{{$sector_array[0][2]}}", "{{$sector_array[1][2]}}", "{{$sector_array[2][2]}}", "{{$sector_array[3][2]}}", "{{$sector_array[4][2]}}","{{$sector_array[5][2]}}","{{$sector_array[7][2]}}","{{$sector_array[8][2]}}","{{$sector_array[6][2]}}"],
                backgroundColor: [
                    "#36A2EB",
                    "#FF6384",
                    "#FF9F40",
                    "#FFCD56",
                    "#4BC0C0",
                    "#edff00",
                    "#00ffa3",
                    "#f53b5c",
                    "#b73bf5",
                ],
                borderColor: [
                    "#CDA776",
                    "#FF6384",
                    "#FF9F40",
                    "#FFCD56",
                    "#4BC0C0",
                    "#edff00",
                    "#00ffa3",
                    "#f53b5c",
                    "#b73bf5",
                ],
                borderWidth: [1, 1, 1, 1, 1]
            }
        ]
    };

    var options = {
        responsive: true,
        title: {
            display: true,
            position: "top",
            text: "CURRENT ALLOCATION",
            fontSize: 15,
            fontColor: "#111"
        },
        legend: {
            display: true,
            position: "right",
            labels: {
                fontColor: "#333",
                fontSize: 30
            },
         tooltips: {
            titleFontSize: 50
            },
          animation: {
                onComplete: function () {                
                    window.JSREPORT_READY_TO_START = true
                }
            }
        }
    };

    var chart1 = new Chart(ctx1, {
        type: "pie",
        data: data1,
        options: options
    });
    Chart.NewLegend = Chart.Legend.extend({
      afterFit: function() {
        this.height = this.height + 100;
      },
    });

    // var img =document.getElementById("sector-pie-chart").toDataURL();
    // console.log(img);
    // document.getElementById("url").src = img;
  });


</script>
</body>
</html>