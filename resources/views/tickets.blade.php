<?php 
use Carbon\Carbon;  
$today = Carbon::now()->subDays(1);
$dateOrder = date('Y-m-d',strtotime($today));
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

  <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}">
  <script src="{{url('js/jquery.min.js')}}"></script>
  <script src="{{url('js/jquery-ui.js')}}"></script>
  <script src="{{url('js/bootstrap.min.js')}}"></script>
  <link rel="stylesheet" href="{{url('css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{url('css/jqueryui.css')}}">
  <link rel="stylesheet" href="{{url('css/index.css')}}">
  <link rel="stylesheet" href="{{url('css/login-responsive.css')}}">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
      <link rel="stylesheet" href="{{url('css/ticket.css')}}">
  <link rel="stylesheet" href="{{url('css/sector.css')}}">
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
  <style type="text/css">
    .amc-wrapper{
      min-height: 70vh;
      overflow-y: scroll;
    }
  </style>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid" id="navbar_container">
    <div class="navbar-header">
      <a class="navbar-brand" href="/"><img class="logo" src = "{{url('img/Right-repor-logo.svg')}}"/></a>
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <div class="col-xs-2 text-center padding-lr-zero pull-right">
          <div class="dropdown" id="drop">
                <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                <ul class="dropdown-menu">
                  <li><a href="\logout" id="logout">Logout</a></li>
                </ul>
            </div>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div class="container-fluid" id="top-header"> 
    <div class="row"> 
        <div class = "col-lg-12 col-md-12 col-sm-12"> 
            <div class = "col-lg-6 col-md-6 col-sm-6 padding-l-zero">  
                <p><span>Tickets</span></p>
            </div>  
            <div class = "col-lg-3 col-md-3 col-sm-3">  
                <span>Filter by: </span> 
                <div class="dropdown" style="display: inline-block;">
                  <button class="btn btn-primary filter-btn dropdown-toggle f-all" type="button" data-toggle="dropdown" id="filter-btn">
                    @if($val == 0)
                    All
                    @elseif($val == 1)
                    Hold
                    @elseif($val == 2)
                    Progress
                    @elseif($val == 3)
                    Resolved
                    @endif
                    <span class="pull-right"><i class="material-icons">keyboard_arrow_down</i></span></button>
                  <ul class="dropdown-menu" id="filter-dropdown">
                    <li><a href="/tickets/1" class="f-hold" >Hold</a></li>
                    <li><a href="/tickets/3" class="f-resolved" >Resolved</a></li>
                    <li><a href="/tickets/2" class="f-progress" >In Progress</a></li>
                    <li><a href="/tickets" class="f-all" >All</a></li>
                  </ul>
                </div>
            </div>  
            <div class = "col-lg-3 col-md-3 col-sm-3 padding-r-zero">  
                <a href="#" class="btn btn-primary pull-right" id="add-ticket">Add Ticket</a>
            </div>  
        </div>  
    </div>  
</div>

<div class = "container-fluid" id="ticket-header">
<div class="row"> 
    <div class = "col-lg-12 col-md-12 col-sm-12" id="header-holder"> 
        <div class = "col-lg-3 col-md-3 col-sm-3"><p>Client <span class="pull-right">Priority</span></p></div>
        <div class = "col-lg-3 col-md-3 col-sm-3"><p>Description</p></div>
        <div class = "col-lg-3 col-md-3 col-sm-3"><p>Current Status</p></div>
        <div class = "col-lg-3 col-md-3 col-sm-3"><p>Process State</p></div>
    </div>  
</div>  
</div>
@if($count == 0)
<p class="no-ticket-p">No Tickets Found</p>
@else

@foreach($tickets as $ticket)
<?php 
  $current_date = date('Y-m-d',strtotime($ticket['etc']));
?>

<?php if ($ticket['priority']): ?>
  <?php if ($ticket['process_state'] != 1): ?>
    <?php if ($ticket['process_state'] != 3): ?>
      <?php if ($today > $current_date): ?>
        <div class = "container-fluid red-left-border" id="ticket-content-holder">
      <?php else: ?>
        <div class = "container-fluid" id="ticket-content-holder">
      <?php endif ?>
    <?php else: ?>
      <div class = "container-fluid" id="ticket-content-holder">
    <?php endif ?>
  <?php else: ?>
      <div class = "container-fluid" id="ticket-content-holder">

  <?php endif ?>
      
<div class="row"> 
    <div class = "col-lg-12 col-md-12 col-sm-12"> 
        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder">
            <div class = "col-lg-9 col-md-9 col-sm-9 padding-lr-zero ">
              <p class="client-name">{{$ticket['investor_name']}}</p>
              <p class="ticket-time">Ticket Date: {{date('Y-m-d',strtotime($ticket['created_at']))}}</p>
            </div>
            <div class = "col-lg-3 col-md-3 col-sm-3">  
                <!-- <span class="priority-label pull-right">{{$ticket['priority']}}</span> -->
                <input type="number" name="" class="input-field pull-right" data-id="{{$ticket['id']}}" value="{{$ticket['priority']}}">
            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder">

            <p class="data_state">{{$ticket['description']}}</p>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder"><p class="data_state">{{$ticket['current_status']}}</p></div>

        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder" style="border-right: none !important;">
            <div class = "col-lg-8 col-md-8 col-sm-8">
                @if($ticket['process_state'] == 1)  
                <p class="ticket-status hold">HOLD</p>
                @elseif($ticket['process_state'] == 2)
                <p class="ticket-status in-progress">PROGRESS</p>
                @elseif($ticket['process_state'] == 3)
                <p class="ticket-status resolved">RESOLVED</p>
                @else
                <p class="ticket-status">NO STATE</p>
                @endif
                <?php if ($ticket['process_state'] != 1): ?>
                  <?php if ($ticket['process_state'] != 3): ?>                    
                  <?php if ($today > $current_date): ?>
                  <p class="etc">ETC : {{$ticket['etc']}}</p>
                  <?php else: ?>
                  <p class="etc" style="color: gray !important;">ETC : {{$ticket['etc']}}</p>
                  <?php endif ?>
                  <?php endif ?>
                <?php endif ?>
            </div>
            <div class = "col-lg-4 col-md-4 col-sm-4">  
                <div class="dropdown" style="float: right;">
                  <span class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="material-icons">more_vert</i></span>
                  <ul class="dropdown-menu">
                    <!-- <li><a href="#" class="change_priority" data-value="{{$ticket['id']}}" data-pri="{{$ticket['priority']}}">Change Priority</a></li> -->
                    <li><a href="#" class="change_etc" data-value="{{$ticket['id']}}">Change ETC</a></li>
                    <li><a href="#" class="edit_priority" data-value="{{$ticket['id']}}">Edit</a></li>
                    <li><a href="#" class="red delete_priority" data-value="{{$ticket['id']}}">Delete</a></li>
                  </ul>
                </div>
            </div>    
        </div>
    </div>  
</div>  
</div>
<?php endif ?>
@endforeach

@foreach($tickets as $ticket)
<?php 
  $current_date = date('Y-m-d',strtotime($ticket['etc']));
?>
<?php if ($ticket['priority'] == 0): ?>
  <?php if ($ticket['process_state'] != 1): ?>
    <?php if ($ticket['process_state'] != 3): ?>
          <?php if ($today > $current_date): ?>
      <div class = "container-fluid red-left-border" id="ticket-content-holder">
    <?php else: ?>
      <div class = "container-fluid" id="ticket-content-holder">
    <?php endif ?>
  <?php else: ?>
    <div class = "container-fluid" id="ticket-content-holder">
  <?php endif ?>
<?php else: ?>
      <div class = "container-fluid" id="ticket-content-holder">
<?php endif ?>

<div class="row"> 
    <div class = "col-lg-12 col-md-12 col-sm-12"> 
        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder">
            <div class = "col-lg-9 col-md-9 col-sm-9 padding-lr-zero ">
              <p class="client-name">{{$ticket['investor_name']}}</p>
              <p class="ticket-time">Ticket Date: {{date('Y-m-d',strtotime($ticket['created_at']))}}</p>
            </div>
            <div class = "col-lg-3 col-md-3 col-sm-3">  
                <!-- <span class="priority-label pull-right">{{$ticket['priority']}}</span> -->
                <input type="number" name="" class="input-field pull-right" data-id="{{$ticket['id']}}" value="{{$ticket['priority']}}">

            </div>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder">

            <p class="data_state">{{$ticket['description']}}</p>
        </div>
        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder"><p class="data_state">{{$ticket['current_status']}}</p></div>

        <div class = "col-lg-3 col-md-3 col-sm-3 company-name-holder" style="border-right: none !important;">
            <div class = "col-lg-8 col-md-8 col-sm-8">
                @if($ticket['process_state'] == 1)  
                <p class="ticket-status hold">HOLD</p>
                @elseif($ticket['process_state'] == 2)
                <p class="ticket-status in-progress">PROGRESS</p>
                @elseif($ticket['process_state'] == 3)
                <p class="ticket-status resolved">RESOLVED</p>
                @else
                <p class="ticket-status">NO STATE</p>
                @endif
                <?php if ($ticket['process_state'] != 1): ?>
                  <?php if ($ticket['process_state'] != 3): ?>                    
                  <?php if ($today > $current_date): ?>
                  <p class="etc">ETC : {{$ticket['etc']}}</p>
                  <?php else: ?>
                  <p class="etc" style="color: gray !important;">ETC : {{$ticket['etc']}}</p>
                  <?php endif ?>
                  <?php endif ?>
                <?php endif ?>
            </div>
            <div class = "col-lg-4 col-md-4 col-sm-4">  
                <div class="dropdown" style="float: right;">
                  <span class="dropdown-toggle" type="button" data-toggle="dropdown"><i class="material-icons">more_vert</i></span>
                  <ul class="dropdown-menu">
                    <!-- <li><a href="#" class="change_priority" data-value="{{$ticket['id']}}" data-pri="{{$ticket['priority']}}">Change Priority</a></li> -->
                    <li><a href="#" class="change_etc" data-value="{{$ticket['id']}}">Change ETC</a></li>
                    <li><a href="#" class="edit_priority" data-value="{{$ticket['id']}}">Edit</a></li>
                    <li><a href="#" class="red delete_priority" data-value="{{$ticket['id']}}">Delete</a></li>
                  </ul>
                </div>
            </div>    
        </div>
    </div>  
</div>  
</div>
<?php endif ?>
@endforeach

@endif

<div id="addNewTicket" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Add Ticket</h4>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="add_new_ticker_form" enctype="multipart/form-data">
          <div class="form-group" id="ticket-name">
              <select class="mont-reg" name="inv_name" id = "inv_name" required>
              @foreach($names as $name)
                <option value="{{$name}}">{{$name}}</option>
              @endforeach
            </select>
            <span id="newuser">Add NewUser</span>
          </div>

          <div class="form-group">
<!--               <select class="mont-reg" name="priority" id = "priority_id">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select> -->
            <input type="number" max="99" name="priority" id="priority_id" required placeholder="Priority" value="0">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="etc" id = "etc_id" required placeholder="ETC">
          </div>

          <div class="form-group">
            <textarea rows="5" cols="40" class="mont-reg" name="description" id="description_id" required placeholder="Description"></textarea>
          </div>

          <div class="form-group">
            <input type="submit" name="add_ticket_btn" id = "add_ticket_btn" class="btn btn-primary blue-btn center-block" value="Add">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>

<div id="editTicket" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Edit</h4>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="edit_ticket_form" enctype="multipart/form-data">
          <input type="hidden" name="id" id="ticket-id" value="">

          <div class="form-group" id="current-status">
            <textarea rows="5" cols="40" class="mont-reg" name="current_status" id="current_value" required placeholder="Current Status"></textarea>
          </div>

          <div class="form-group">
              <select class="mont-reg" name="status" id = "status_id">
                <!-- <option value="0">NO STATUS</option> -->
                <option value="1">HOLD</option>
                <option value="2">PROGRESS</option>
                <option value="3">RESOLVED</option>
            </select>
          </div>

          <div class="form-group">
            <input type="submit" name="edit_ticket_btn" id = "edit_ticket_btn" class="btn btn-primary blue-btn center-block" value="EDIT">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>

<div id="adminPasswordModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Admin Password</h4>
        <p class="text-center" id="admin_text">Admin Password is required to Change Action.</p>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id_val" value="">
          <input type="hidden" name="old_val" id="old_val" value="">
          <div class="form-group" id="password_area">
            <input type="password" class="mont-reg" name="admin_password" id = "admin_password" required placeholder="Enter Admin Password">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "check_admin_pass" class="btn btn-primary blue-btn" value="Change">
          </div>

      </div>
    </div>

  </div>
</div>

<div id="adminPasswordModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Admin Password</h4>
        <p class="text-center" id="admin_text">Admin Password is required to Change Action.</p>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id_val2" value="">
          <div class="form-group" id="password_area">
            <input type="password" class="mont-reg" name="admin_password" id = "admin_password2" required placeholder="Enter Admin Password">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "check_admin_pass2" class="btn btn-primary blue-btn" value="Change">
          </div>

      </div>
    </div>

  </div>
</div>

<div id="adminPasswordModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Admin Password</h4>
        <p class="text-center" id="admin_text">Admin Password is required to Change Action.</p>
      </div>
      <div class="modal-body">
          <input type="hidden" name="id" id="id_val3" value="">
          <div class="form-group" id="password_area">
            <input type="password" class="mont-reg" name="admin_password" id = "admin_password3" required placeholder="Enter Admin Password">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "check_admin_pass3" class="btn btn-primary blue-btn" value="Delete">
          </div>

      </div>
    </div>

  </div>
</div>

<div id="changePriority" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Update Priority</h4>
      </div>
      <div class="modal-body">
        <select class="mont-reg" name="new_val" id = "priority_change">
          <!-- <option value="0">0</option> -->
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>

      <div class="form-group">
        <input type="submit" name="change_btn" id = "change_btn" class="btn btn-primary blue-btn center-block" value="Update">
      </div>

      </div>
    </div>
  </div>
</div>

<div id="changedate" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Update Date</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <input type="text" class="mont-reg" name="new_date" id = "new_date" required placeholder="Select New Date">
          </div>

      <div class="form-group">
        <input type="submit" name="change_btn" id = "change_date" class="btn btn-primary blue-btn center-block" value="Update">
      </div>

      </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="/js/tickets.js">
    var tickets = <?php echo json_encode($tickets);?>;
</script>

</body>
</html>
