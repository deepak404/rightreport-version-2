<!DOCTYPE html>
<html lang="en">
<head>
  <title>RightReport</title>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

  <link rel="stylesheet" href="css/bootstrap.min.css">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/index.css">
  <link rel="stylesheet" href="css/login-responsive.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
  <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
</head>
<body>

<nav class="navbar">
  <div class="container-fluid" id="navbar_container">
    <div class="navbar-header">
      <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
      <a class="navbar-brand" href="#"><img src = "img/Right-repor-logo.svg"/></a>
    </div>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <div class="col-xs-3 "><p class="img-circle" id = "profile_text">V</p></div>
        <div class="col-xs-2 padding-lr-zero">
          <span id = "user_name">Vasudev</span>
        </div>
        <div class="col-xs-2 text-center padding-lr-zero">
          <div class="dropdown" id="drop">
                <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                <ul class="dropdown-menu">
                  <li><a href="#" id="settings">Setting</a></li>
                  <li><a href="#" id="logout">Logout</a></li>
                </ul>
            </div>
        </div>
      </li>
    </ul>
  </div>
</nav>

<div class = "container-fluid">

<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-3" id="sidebar_wrapper">
    <!--<div class="row">-->
      <div id="sidebar">
        <div class="col-xs-12" id="op_bar">
          <div class="col-xs-4 text-center"><a href="" class="text-center"><i class="material-icons top_bar_icons">group_add</i></a></div>
          <div class="col-xs-4 text-center"><a href="" class="text-center"><i class="material-icons top_bar_icons">person_add</i></a></div>

          <div class="col-xs-4 text-center"><a href="" class="text-center"><i class="material-icons top_bar_icons">search</i></a></div>
        </div>


        <div class="col-xs-12 padding-lr-zero client_bar">
          <div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><span class="client_name mont-reg">Vasudev</span></div></div>
          <div class="col-xs-4 text-center">
              <i class="material-icons side_icon dot_btn dropdown-toggle" data-toggle = "dropdown" >more_horiz</i>
                  <ul class="dropdown-menu sub_person_option">
                    <li><a href="#" id="settings">Add Person</a></li>
                    <li><a href="#" id="settings">Rename</a></li>
                    <li><a href="#" id="logout" class="remove">Remove</a></li>
                  </ul>

              <span><a href="#debttypes" data-toggle = "collapse" class=""><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>                  
          </div>
        </div>

        <div class="col-xs-12 padding-lr-zero collapse" id="debttypes">

                <div class="sub_person_bar col-xs-12">
                  <div class="col-xs-10">                   
                      <a href="" class="sub-menu sub-person">Naveen</a>
                  </div>
                  <div class="col-xs-2">
                    <span><a href="" class=""><i class="material-icons side_icon dot_btn">more_horiz</i></a></span>
                  </div>
                </div>

                <div class="sub_person_bar col-xs-12">
                  <div class="col-xs-10">                   
                      <a href="" class="sub-menu sub-person">Naveen</a>
                  </div>
                  <div class="col-xs-2">

                      <div class="dropdown" id="drop">
                      <i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>
                          <ul class="dropdown-menu sub_person_option">
                            <li><a href="#" id="settings">Rename</a></li>
                            <li><a href="#" id="logout" class="remove">Remove</a></li>
                          </ul>
                      </div>



                    
                  </div>
                </div>


        </div>

        <div class="col-xs-12 padding-lr-zero client_bar">
          <div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><span class="client_name mont-reg">Vasudev</span></div></div>
          <div class="col-xs-4 text-center">
              <span><a href="" class=""><i class="material-icons side_icon dot_btn">more_horiz</i></a></span>
              <span><a href="" class=""><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>
          </div>
        </div>




      </div>
    <!--</div>-->
  </div>
  <div class="col-lg-9 col-md-9 col-sm-9" id="contentbar_wrapper">
    <div id="contentbar">
    <div class="col-lg-12 col-md-12 col-sm-12" id="client_relbar">
      <p id="client_det"><span id="client_parent">Vasudev</span><span><img src="icons/arrow.png"/></span><span id="client_child">Consolidated Investment Summary</span></p>

      <div id="scheme_export">
        <button type="button" class="btn btn-primary" id="add_scheme_btn">Add Scheme</button>
        <button type="button" class="btn btn-primary" id="export_btn">Export</button>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12" id="invdetails_bar">
      <div class="col-lg-3 col-md-3 col-sm-3"><p class="content_header">Amount Invested</p><p class="amount_header">Rs. 15,00,00,000</p></div>
      <div class="col-lg-3 col-md-3 col-sm-3"><p class="content_header">Current Market Value</p><p class="amount_header">Rs. 15,00,00,000</p></div>
      <div class="col-lg-3 col-md-3 col-sm-3"><p class="content_header">Unrealised Profit/loss</p><p class="amount_header">Rs. 15,00,00,000</p></div>
      <div class="col-lg-3 col-md-3 col-sm-3"><p class="content_header">Absolute Returns</p><p class="amount_header">Rs. 15,00,00,000</p></div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 padding-lr-zero">
      <div class="table-wrapper" id="cis_wrapper">
        <table class="table table-bordered">
        <thead>
          <tr>
            <th><p class="table_heading">Scheme Name</p></th>
            <th><p class="table_heading">Amt. Invested</p></th>
            <th><p class="table_heading">Current Mrkt Val.</p></th>
            <th><p class="table_heading">Unrealised Profit/Loss</p></th>
            <th><p class="table_heading">Absolute Returns</p></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>
          </tr>
         <tr>
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>
          </tr>
          <tr>
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>
          </tr>

          <tr>
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>
          </tr>

          <tr>
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>
          </tr>

          <tr>
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>
          </tr>

          <tr style="background-color: #F2F9FF;">
            <td class="scheme_name"><p class="table_data scheme_name_p">ICICI prudent fund regular growth</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">50,00,00,000</p></td>
            <td><p class="table_data">5,00,00,000</p></td>
            <td><p class="table_data">50%</p></td>

          </tr>

        </tbody>
      </table>
      </div>
    </div>

    </div>
  </div>
</div>


</div>


<!-- Modals Begin below -->


<div id="addSchemeModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Add Scheme</h4>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="add_scheme_form">
          <div class="form-group">
            <input type="text" class="mont-reg" name="scheme_name" id = "scheme_name" required placeholder="Enter Scheme Name">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="scheme_type" id = "scheme_type" required placeholder="Enter Scheme type">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="dop" id = "dop" required placeholder="Enter Date of Purchase">
          </div>

          <div class="form-group">
            <input type="number" class="mont-reg" name="amt_inv" id = "amt_inv" required placeholder="Amount Invested">
          </div>

          <div class="form-group">
            <input type="number" class="mont-reg" name="purchase_nav" id = "purchase_nav" required placeholder="Purchase Nav">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Add">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>



<div id="settingsModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal_header">Settings</h4>
        <p class="text-center" id="change_pass">Change Password</p>
      </div>
      <div class="modal-body">
        <form method="#" action="#" id="change_password_form">
          <div class="form-group">
            <input type="text" class="mont-reg" name="old_password" id = "old_password" required placeholder="Old Password">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="new_password" id = "new_password" required placeholder="New password">
          </div>

          <div class="form-group">
            <input type="text" class="mont-reg" name="repeat_password" id = "repeat_password" required placeholder="Repeat New Password">
          </div>

          <div class="form-group">
            <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Save">
          </div>

        </form>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  $('#add_scheme_btn').on('click',function(){
      $('#addSchemeModal').modal('show');
  });

</script>


</body>
</html>
