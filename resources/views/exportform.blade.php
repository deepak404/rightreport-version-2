<!DOCTYPE html>
<html>
<head>
	<title>Common Transaction slip</title>
	<style type="text/css">
		html{
			margin-bottom: 0px;
			margin-top: 0px;
		}
		.address-holder{
			width: 100% !important;
			padding-right: 10px;
			/*margin-top: 10px;*/
			font-size: 12px; 
		}
		.address{
		    text-align: right;
		    font-size: 12px; 
		}

		.address p{
			margin: 2px 10px ;
			display: block;
		}

		.text-center{
			text-align: center;
		}

		body{
			padding: 20px 30px;
		}

		#header{
			text-align: center;
		}

		p{
			margin: 0px;
		}

		#arn-holder p {
			font-weight: bold;
			margin-bottom: 3px;
			font-size: 14px;
		}

		#dist-det{
			font-size: 14px;
			padding: 10px 0px 10px;
		}

		#dist-det p{
			display: block;
			margin: 10px 0px;

			
		}

		#dist-det p>span{
			border-bottom: 1px solid gainsboro;
			padding-bottom: 2px;
			width: 40%;
			display: inline-block;
			margin-left: 20px;
		}

		.bold{
			font-weight: bold;
			padding-bottom: 10px;
		}

		.bold span{
			font-weight: normal;
		}

		.heading{
			font-size: 14px;
			font-weight: bold;
		}

		.w-80{
			width: 75%;
			display: inline-block;
			font-size: 14px !important;
		}

		.w-20{
			width: 20%;
			display: inline-block;
			font-size: 14px !important;
		}

		.w-100{
			width: 100%;
			font-size: 14px!important;
		}

		.w-80 >span{
			width: 75%;
    		display: inline-block;
		}

		.w-20 > span{
			width: 65%;
			display: inline-block;
		}

		.w-100 > span{
			width: 75%;
			display: inline-block;
			margin-left: 10px;
		}

		.info{
			margin-top: 10px;
			margin-bottom: 20px;
		}

		.underline{
			border-bottom: 1px solid gainsboro;
			padding-bottom: 2px;
			
		}


		.trans-type{
			font-size: 14px;
			/*margin: 10px auto;*/
			/*margin-top: 50px;*/
			margin-bottom: 50px;
		}


		.trans-type:first-child{
			margin-top: 25px!important;
		}

		.w-80 > span, .w-20 > span{
			margin-left: 15px;
		}

		.input-holder{
			margin-bottom: 10px;
		}

		.w-20 > .underline-div{
			width: 65%;
			margin-bottom: -5px;
			margin-top: 5px;
		}

		.trans-type{
			margin-bottom: 20px;
		}

		#switch-div > span:first-child{
			width: 40%;
			display: inline-block;
		}

		#switch-div > span:nth-child(2){
			width: 30%;
			display: inline-block;
		}

		#app-signs span{
			/*width: 1.33%;*/
			display: inline-block;
			font-size: 14px !important;
			margin: 10px auto;
		}

		.underline-div{
			display: inline-block;
			width: 80%;
			padding-bottom: 2px;
			border-bottom: 1px solid gainsboro;
			margin-left: 10px;
			margin-bottom: -5px;
			margin-top: 5px;
		}

		.w-100 > .underline-div{
			width: 75%;	
		} 


		/*.w-100 > .underline-div >span{
			display: inline-block;
			padding-bottom: 10px;
		} */


		#switch-div .underline-div:first-child{
			width: 20%;
			margin-bottom: -10px;
			margin-top: 15px;
		}

		#switch-div .underline-div:nth-child(2){
			width: 30%;
			margin-bottom: -10px;
			margin-top: 15px;
		}

		h3{
			margin: 0px;
		}

		.inv-det{
			/*padding-top: 5px !important;*/
			padding-bottom: 10px;
			/*padding-left: 10px;*/
			margin-top: 10px;
		} 

		.app-signs{
			display: inline-block;
			width: 33%;
			margin-top: 10px;
			text-align: center;
			font-size: 14px;

		}

		#dist-div .underline-div{
			width: 70%;
			margin-bottom: -10px;
			margin-top: 15px;
		}

		#dist-div p{
			display: block;
		}

		#dist-div p>.underline-div{
			display: inline-block;
			margin-bottom: 0px;
			padding-top: 20px;
		}

		#distributor-name span.underline-div > span{
			display: inline-block;
			margin-bottom: -20px;
		}
		.det-page{
			display: inline-block !important;
		}
		.ml-5{
			margin-left: 8px;
		}

		#bank-and-branch-div{
			width: 95%;
		}

		#scheme-name-div{
			width: 76%;
			margin-right: 5px;
		}
		#cheque-div{
			width: 74%;
			margin-right: 5px;
		}

		#switch-rs-div{
			width: 70%;
		}

		#or-div{
			width: 50%;	
			margin-right: 5px;
			display: inline-block;
			padding-right: 5px;
		}

		#or-div .underline-div{
			width:30%;
		}

		#sign-holder{
			margin-top: 50px;
		}

	</style>
</head>
<body>
	
		<div class="address">
			<div class="address-holder">
				<p>Level 1, No 1, Balaji first Avenue,</p>
				<p>T.Nagar, Chennai</p>
				<p>Tamil Nadu, India</p>
				<p>PIN: 600017</p>
				<p>Ph: +91 9843051487</p>
			</div>
		</div>

		<h3 id="header">COMMON TRANSACTION SLIP</h3>

		<div id="arn-holder">
			<p>ARN : 116221</p>
			<p>EUIN NO : E173580</p>
		</div>
		
		<div id = dist-det>
			<div><p class="det-page" id="distributor-name">Distributor Name : </p><span class="underline-div"><span>Vasudev Fatehpuria</span></span></div>
			<div><p class="det-page">Folio Number : </p><span class="underline-div"><span>{{$selected['folio_number']}}</span></span></div>
			<div><p class="det-page">AMC : </p><span class="underline-div"><span>{{$selected['amc']}}</span></span></div>
		</div>

		<div class="inv-det">
			<div><p class="bold det-page">Investor Name : <span>{{$selected['investor_name']}}</span></p></div>
			<div><p class="bold det-page">PAN Number : <span>{{$selected['investor_pan']}}</span></p></div>
		</div>

		<div class="trans-type">
			<p class="heading">Additional Purchase</p>
			<p class="info">I/we would like to purchase additional units as per the following details:</p>

			<div class="input-holder">
				<div class="w-80">Scheme Name: <div class="underline-div" id="scheme-name-div"><span></span></div></div>
				<div class="w-20">Option: <div class="underline-div"><span></span></div></div>
			</div>
			<div class="input-holder">
				<div class="w-80">Cheque Number: <div class="underline-div" id="cheque-div"><span></span></div></div>
				<div class="w-20 ml-5">Date: <div class="underline-div"><span></span></div></div>
			</div>

			<div class="input-holder">
				<div class="w-80">Bank and Branch Number: <div class="underline-div" id="bank-and-branch-div"><span></span></div></div>
			</div>

			<div class="input-holder">
				<div class="w-80">Amount(Rs): <div class="underline-div" id="bank-and-branch-div"><span></span></div></div>
			</div>
		</div>

		<!--Additional Purchase ends -->


		<div class="trans-type">
			<p class="heading">Switch Request</p>
			<p class="info">I/we would like to switch the units as per the following details:</p>

			<div class="input-holder">
				<div class="w-80">Scheme Name: <div class="underline-div" id="scheme-name-div"><span></span></div></div>
				<div class="w-20">Option: <div class="underline-div"><span></span></div></div>
			</div>
			<div class="input-holder">
				<div class="w-80">To Scheme : <div class="underline-div"><span class="underline"></span></div></div>
				<div class="w-20">Option: <div class="underline-div"><span></span></div></div>
			</div>

			<!-- <div class="input-holder">
				<div class="w-100" id="switch-div">I wish to switch Rs. <div class="underline-div"><span class="underline"></span></div> or <div class="underline-div"><span class="underline"></span></div>Units</div>
			</div> -->

			<div class="input-holder">
				<div class="w-80">I wish to switch Rs.: <div class="underline-div" id="switch-rs-div"><span></span></div></div>
				<div class="" id="or-div">Or <div class="underline-div"><span></span></div>Units</div>
			</div>
		</div>

		<!-- Switch request ends -->


		<div class="trans-type">
			<p class="heading">Redemption Request</p>
			<p class="info">Please redeem my/our amounts as per the following details:</p>

			<div class="input-holder">
				<div class="w-80">Scheme Name: <div class="underline-div" id="scheme-name-div"><span>{{$selected['scheme_name']}}</span></div></div>
				<div class="w-20">Option: <div class="underline-div"><span>Regular - {{$selected['option']}}</span></div></div>
			</div>

			<!-- <div class="input-holder">
				<div class="w-100" id="switch-div">Please redeem Rs. <div class="underline-div"><span class="underline"></span></div> or <div class="underline-div"><span class="underline"></span></div>Units from the scheme.</div>
			</div> -->

			<div class="input-holder">
				<div class="w-80">Please Redeem Rs. <div class="underline-div" id="switch-rs-div"><span></span></div></div>
				<div class="" id="or-div">Or <div class="underline-div"><span>ALL</span></div>Units</div>
			</div>
		</div>

		<!-- Redemption request ends -->


		<div id="signatures">
			<p>Signature(s)</p>
			<div id="sign-holder">
				<p class="app-signs">1<sup>st</sup>Applicant</p>
				<p class="app-signs">2<sup>nd</sup>Applicant</p>
				<p class="app-signs">3<sup>rd</sup>Applicant</p>
			</div>
		</div>



</body>
</html>