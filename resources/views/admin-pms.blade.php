

<!DOCTYPE html>
<html lang="en">
<head>
    <title>RightReport</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/login-responsive.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/jqueryui.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://use.fontawesome.com/8fa68942ad.js"></script>
    {{--<script src="js/admin.js"></script>--}}
    <script src="js/loader.js"></script>
    <script src="js/admin-pms.js"></script>
</head>
<body>
<div class="loader" id="loader" style="display: none;"></div>
<nav class="navbar">
    <div class="container-fluid" id="navbar_container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#" id="sidebar_icon"><img src = "icons/sidebar.png"/></a>
            <a class="navbar-brand" href="#"><img class="logo" src = "img/Right-repor-logo.svg"/></a>
            <a href="/admin_bonds" class="module-links">Bonds</a>
            <a href="/admin_pms" class="module-links">PMS</a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="/export_form" target="_blank" style="float: right;"><i class="material-icons">format_align_left</i></a></li>
            <li>
                @if($seen_status == 1)
                    <div class="col-xs-3 notification-icon" id="notification"><i class="material-icons" id="note-icon">notifications</i><sup id="svg"><svg xmlns="http://www.w3.org/2000/svg" fill="#000000" height="10" viewBox="0 0 10 10" width="10"><path d="M24 24H0V0h24v24z" fill="none"/><circle cx="5" cy="5" fill="red" r="5"/></svg></sup>
                    </div>
                @else
                    <div class="col-xs-3 notification-icon" id="notification"><i class="material-icons" id="note-icon">notifications</i><sup id="svg"></sup>
                    </div>
                @endif
                <div id="notifications" >
                    <ul id="notification-ul">
                        <li>
                            <div id="notification-master" style="background-color: #f1f8fd">
                                <div id="notification-div">
                                    <p id="p-investorname">Investor name</p>
                                    <p id="p-fundname">Sundaram Long Term Micro Cap Tax Advantage Fund Series V (Regular Plan Growth Option)</p>
                                    <span id="span-amount">Rs.5,00,00,000</span>
                                    <span id="span-time">12min ago</span>
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
                <div class="col-xs-5 padding-lr-zero">
                    <span id = "user_name">{{\Auth::user()->name}}</span>
                </div>
                <div class="col-xs-2 text-center padding-lr-zero">

                    <div class="dropdown" id="drop">
                        <i class="fa fa-ellipsis-v dropdown-toggle" id="main_menu" data-toggle = "dropdown"></i>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="settings">Setting</a></li>
                            <li><a href="{{url('/logout')}}" id="logout">Logout</a></li>
                            <li><a id="add_new_scheme">Add Scheme</a></li>
                            <li><a id="add_pms_corp">Add PMS Corp.</a></li>
                            <li><a id="add_pms_scheme">Add PMS Scheme</a></li>
                            <li><a id="access">Access</a></li>
                            <li><a id="add_new_user">Add User</a></li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</nav>


<div class = "container-fluid">

    <div class="row">
        <div class="col-lg-3 col-md-3" id="sidebar_wrapper">
            <!--<div class="row">-->
            <div id="sidebar">

                <div class="col-xs-12" id="op_bar">
                    <p class="aum-container"></p>
                    {{--<p class="text-center aum-container"><span id="aum-text">AUM - </span>Rs.<span id = "aum">{{$aum}}</span></p>--}}
                </div>

                <div class="col-xs-12" id="op_bar">
                    <div class="col-xs-4 text-center"><a href="/admin_getAmc" target="_blank"><i class="material-icons">account_balance</i></a></div>

                    {{--<div class="col-xs-3 text-center"><a href="#" id = "set_date" class="text-center "><i class="material-icons top_bar_icons">today</i></a></div>--}}

                    <div class="col-xs-4 text-center"><a href="/admin_portfolio" target="_blank""><i class="material-icons">pie_chart</i></a></div>

                    <div class="col-xs-4 text-center"><a href="/tickets" target="_blank""><i class="material-icons">style</i></a></div>
                </div>






                <div id="client_wrapper" class="col-xs-12 client_wrapper">
                    <!-- user starts -->
                    @foreach($users as $user)
                        <div class="col-xs-12 padding-lr-zero client_bar">

                            <div class="col-xs-8 padding-lr-zero"><div class="col-xs-12"><a data-id = "{{$user->id}}" data-groupname="{{$user->id}}" class="client_name group_name mont-reg group">{{$user->name}}</a></div></div>

                            <div class="col-xs-4 text-center">
                                <span><a href="#user{{$user->id}}" data-toggle = "collapse" class="side_icon_parent user_side_icon_parent"><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>
                            </div>

                        </div>

                        <!-- user client starts -->

                        <div class="col-xs-12 padding-lr-zero collapse " id="user{{$user->id}}">


                            <!--User member Starts -->

                            @foreach($groups as $group)
                                @if($group->user_id == $user->id)
                                    <div class="col-xs-12 padding-lr-zero client_bar">
                                        <div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="#" class=""><i class="material-icons side_icon admin-client-bar">group</i></a></div><div class="col-xs-10"><a data-id = "1234" data-groupname="{{$group->id}}" class="client_name mont-reg group admin-client-bar">{{$group->name}}</a></div></div>
                                        <div class="col-xs-4 text-center">
                                            <span><a href="#group{{$group->id}}" data-toggle = "collapse" class="side_icon_parent"><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>
                                        </div>
                                    </div>


                                    <div class="col-xs-12 padding-lr-zero collapse admin-group-members" id="group{{$group->id}}">


                                        @foreach($group_members as $group_member)
                                            @if($group_member->group_id == $group->id)
                                                <div class="sub_person_bar col-xs-12">
                                                    <div class="col-xs-10 padding-b-10">
                                                        {{--<a href="#" class="sub-menu investor-name sub-person" data-groupid="10" data-id="24" data-type="group_member">Shyam Prakash Fatehpuria</a>--}}
                                                        <a href="#" class="sub-menu sub-person investor-name" data-groupid = "{{$group->id}}" data-id = "{{$group_member->id}}" data-type = "group_member" >{{$group_member->member_name}}</a>
                                                    </div>
                                                    <div class="col-xs-2 padding-lr-zero">
                                                        {{--<a href="/person/{{$group_member->id}}" target="_blank"><i class="material-icons">pie_chart</i></a>--}}
{{--                                                        <a href="/bmGroupmember/{{$group_member->id}}" target="_blank"><i class="material-icons">equalizer</i></a>--}}
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="dropdown" id="drop">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach





                                        <div class="sub_person_bar col-xs-12">
                                            <div class="col-xs-10 cis_div">
                                                <a href="#" class="cis" data-groupid = "{{$group->id}}" data-groupname = "{{$group->name}}">CIS</a>
                                            </div>
                                            <div class="col-xs-2 padding-lr-zero">
                                                {{--<a href="/id/{{$group->id}}" target="_blank" class="portfolioGroup" data-groupid = "{{$group->id}}"> <i class="material-icons">pie_chart</i></a>--}}
                                                {{--<a href="/bmCIS/{{$group->id}}" target="_blank"><i class="material-icons">equalizer</i></a>--}}
                                            </div>
                                        </div>


                                    </div>
                                @endif
                            @endforeach


                        <!-- User memeber ends -->



                            <!-- Induvidual person starts -->


                            @foreach($persons as $person)

                                @if($person->user_id == $user->id)
                                    <div class="sub_person_bar col-xs-12">

                                        <div class="col-xs-10 padding-l-zero">
                                            <div class="col-xs-2 padding-l-zero">
                                                <i class="material-icons">person</i>
                                            </div>
                                            <div class="col-xs-10 padding-l-zero">
                                                {{--<a href="#" data-id = "{{$person->id}}" class="sub-menu investor-name individual ind_per" data-id = "{{$members->id}}" data-type = "individual">{{$person->name}}</a>--}}
                                                <a href="#" data-id = "{{$person->id}}" class="sub-menu individual investor-name ind_per" data-id = "{{$person->id}}" data-type="individual">{{$person->name}}</a>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 padding-lr-zero">
                                            {{--<a href="/person/{{$person->id}}" target="_blank"><i class="material-icons">pie_chart</i></a>--}}
                                            {{--<a href="/bmSingle/{{$person->id}}" target="_blank"><i class="material-icons">equalizer</i></a>--}}
                                        </div>
                                        <!--                           <div class="col-xs-2">

                                                                      <div class="dropdown" id="drop">
                                                                      </div>
                                                                  </div> -->
                                    </div>
                            @endif
                        @endforeach


                        <!-- Induvidual person ends -->






                        </div>
                        <!--User Client ends -->
                @endforeach

                <!-- user ends -->
                    <div class="sub_person_bar col-xs-12">
                        <div class="col-xs-8 padding-l-zero">
                            <div class="col-xs-2 padding-l-zero">
                                <i class="material-icons">trending_up</i>
                            </div>
                            <div class="col-xs-10 padding-l-zero">
                                <a>SENSEX</a>
                            @if ($sensex_val < 0)
                                <!-- <i id="sensex-icon" class="material-icons" style="color:red">arrow_drop_down</i> -->
                                    <span id="sensex" style="color:red">{{$sensex_val}} %</span>
                            @else
                                <!-- <i id="sensex-icon" class="material-icons" style="color:green">arrow_drop_up</i> -->
                                    <span id="sensex" style="color:green">{{$sensex_val}} %</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="sub_person_bar col-xs-12">
                        <div class="col-xs-8 padding-l-zero">
                            <div class="col-xs-2 padding-l-zero">
                                <i class="material-icons">trending_up</i>
                            </div>
                            <div class="col-xs-10 padding-l-zero">
                                <a>NIFTY</a>
                            @if ($nifty_val < 0)
                                <!-- <i id="nifty-icon" class="material-icons" style="color:red">arrow_drop_down</i> -->
                                    <span id="nifty" style="color:red">{{$nifty_val}} %</span>
                            @else
                                <!-- <i id="nifty-icon" class="material-icons" style="color:green">arrow_drop_up</i> -->
                                    <span id="nifty" style="color:green">{{$nifty_val}} %</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="sub_person_bar col-xs-12">
                        <div class="col-xs-8 padding-l-zero">
                            <div class="col-xs-2 padding-l-zero">
                                <i class="material-icons">timeline</i>
                            </div>
                            <div class="col-xs-10 padding-l-zero">
                                <a>S&P Small cap</a>
                                @if ($smallcap_val < 0)
                                    <span id="smallcap" style="color:red">{{$smallcap_val}} %</span>
                                @else
                                    <span id="smallcap" style="color:green">{{$smallcap_val}} %</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="sub_person_bar col-xs-12">
                        <div class="col-xs-8 padding-l-zero">
                            <div class="col-xs-2 padding-l-zero">
                                <i class="material-icons">timeline</i>
                            </div>
                            <div class="col-xs-10 padding-l-zero">
                                <a>S&P Mid cap</a>
                                @if ($midcap_val < 0)
                                    <span id="midcap" style="color:red">{{$midcap_val}} %</span>
                                @else
                                    <span id="midcap" style="color:green">{{$midcap_val}} %</span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>


                <!-- Group Head starts -->
                <!--<div class="col-xs-12 padding-lr-zero client_bar">
                     <div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="#" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><a data-id = "1234" data-groupname="1234" class="client_name mont-reg group">Group Name</a></div></div>
                     <div class="col-xs-4 text-center">
                         <i class="material-icons side_icon dot_btn dropdown-toggle" data-toggle = "dropdown" >more_horiz</i>
                             <ul class="dropdown-menu sub_person_option">
                               <li><a href="#" class="add_group_person" data-id = "1234">Add Person</a></li>
                               <li><a href="#" class="group_rename" data-id = "1234">Rename</a></li>
                               <li><a href="#" id="logout" class="remove" data-id = "1234">Remove</a></li>
                             </ul>

                         <span><a href="#hellno" data-toggle = "collapse" class=""><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>
                     </div>
                 </div>-->

                <!-- Group Head ends -->

                <!-- group members starts -->

                <!-- <div class="col-xs-12 padding-lr-zero collapse" id="hellno">


                               <div class="sub_person_bar col-xs-12">
                                       <div class="col-xs-10">
                                           <a href="#" class="sub-menu sub-person" data-groupid = "1234" data-subid = "123" data-subperson = "1234">Sub member Name</a>
                                       </div>
                                       <div class="col-xs-2">
                                         <div class="dropdown" id="drop">
                                           <i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>
                                               <ul class="dropdown-menu sub_person_option">
                                                 <li><a href="#" id="rename" class="sub_person_rename" data-id = "#">Rename</a></li>
                                                 <li><a href="#" id="remove" class="sub_person_remove" data-id = "#">Remove</a></li>
                                               </ul>
                                           </div>
                                       </div>
                               </div>



                     <div class="sub_person_bar col-xs-12">
                               <div class="col-xs-10 cis_div">
                                   <a href="#" class="cis" data-groupid = "cis">CIS</a>
                               </div>
                               <div class="col-xs-2">

                               </div>
                     </div>


                </div>-->
                <!-- group members ends -->


                <!--<div class="col-xs-12 padding-lr-zero client_bar">
                  <div class="col-xs-8 padding-lr-zero"><div class="col-xs-2"><a href="" class=""><i class="material-icons side_icon">group</i></a></div><div class="col-xs-10"><span class="client_name mont-reg">Vasudev</span></div></div>
                  <div class="col-xs-4 text-center">
                      <i class="material-icons side_icon dot_btn dropdown-toggle" data-toggle = "dropdown" >more_horiz</i>
                          <ul class="dropdown-menu sub_person_option">
                            <li><a href="#" id="settings">Add Person</a></li>
                            <li><a href="#" id="settings">Rename</a></li>
                            <li><a href="#" id="logout" class="remove">Remove</a></li>
                          </ul>

                      <span><a href="#debttypes" data-toggle = "collapse" class=""><i class="material-icons side_icon key_right">keyboard_arrow_right</i></a></span>
                  </div>
                </div>

                <div class="col-xs-12 padding-lr-zero collapse" id="debttypes">

                        <div class="sub_person_bar col-xs-12">
                          <div class="col-xs-10">
                              <a href="" class="sub-menu sub-person">Naveen</a>
                          </div>
                          <div class="col-xs-2">
                            <span><a href="" class=""><i class="material-icons side_icon dot_btn">more_horiz</i></a></span>
                          </div>
                        </div>

                        <div class="sub_person_bar col-xs-12">
                          <div class="col-xs-10">
                              <a href="" class="sub-menu sub-person">Naveen</a>
                          </div>
                          <div class="col-xs-2">

                              <div class="dropdown" id="drop">
                              <i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>
                                  <ul class="dropdown-menu sub_person_option">
                                    <li><a href="#" id="settings">Rename</a></li>
                                    <li><a href="#" id="logout" class="person_remove">Remove</a></li>
                                  </ul>
                              </div>
                          </div>
                        </div>


                </div>-->

                <!-- Induvidual person starts -->



                <!--<div class="sub_person_bar col-xs-12">

                  <div class="col-xs-10 padding-l-zero">
                    <div class="col-xs-2 padding-l-zero">
                      <i class="material-icons">person</i>
                    </div>
                    <div class="col-xs-10 padding-l-zero">
                      <a href="#" data-id = "123" class="sub-menu individual ind_per" data-person = "123">Person Name</a>
                    </div>
                  </div>
                  <div class="col-xs-2">

                      <div class="dropdown" id="drop">
                      <i class="material-icons side_icon dropdown-toggle dot_btn sub_person_edit" id="main_menu" data-toggle = "dropdown">more_horiz</i>
                          <ul class="dropdown-menu sub_person_option">
                            <li><a href="#" id="rename" class="person_rename" data-id = "#">Rename</a></li>
                            <li><a href="#" id="remove" class="person_remove" data-id = "#">Remove</a></li>
                          </ul>
                      </div>
                  </div>
                </div>-->



                <!-- Induvidual person ends -->




            </div>
            <!--</div>-->
        </div>
        <div class="col-lg-9 col-md-9" id="contentbar_wrapper">
            <div id="contentbar">
                <div class="col-lg-12 col-md-12 " id="client_relbar">
                    <p id="client_det"><span id="client_parent">Group</span><span><img src="icons/arrow.png" id="arrow_img" /></span><span id="client_child">Member</span></p>
                    <p id="client-inv-details"><span id="client-amount-inv"></span><span id="client-current-val"></span>
                    </p>
                    <div id="scheme_export">
                        <button type="button" class="btn btn-primary" id="add_scheme_btn" style="display: none;">Add Scheme</button>
                        {{--<form target="_blank" id="export_form" action="/download_investments" method="POST" style="display: inline;">--}}
                        {{--{{csrf_field()}}--}}
                        {{--<input type="hidden" name="investor_id" value="" id="ex_investor_id" />--}}
                        {{--<input type="hidden" name="investor_type" value="" id="ex_investor_type" />--}}
                        {{--<button type="submit" class="btn btn-primary" id="export_btn">Export</button>--}}

                        {{--</form>--}}


                        <form target="_blank" id="export_form" action="/admin_download_investments" method="POST" style="display: inline;">
                            {{csrf_field()}}
                            <input type="hidden" name="ex_nav_date" value="" id="ex_nav_date" />
                            <input type="hidden" name="investor_id" value="" id="ex_investor_id" />
                            <input type="hidden" name="investor_type" value="" id="ex_investor_type" />
                            {{--<button type="submit" class="btn btn-primary" id="export_btn">Export</button>--}}
                        </form>

                    </div>
                    <span id="all-container">
        <span id="pan_container" style="display: none;">
              <span id="xirr">
              </span>
        </span>
      </span>
                </div>

                <div class="col-lg-12 col-md-12" id="invdetails_bar">
                    <div class="col-lg-4 col-md-4"><p class="content_header">Amount Invested</p><p class="amount_header">Rs. <span id="total_amount_invested">-</span></p></div>
                    <div class="col-lg-4 col-md-4"><p class="content_header">Current Value</p><p class="amount_header">Rs. <span id="current_investment_value">-</span></p></div>
                    <div class="col-lg-4 col-md-4"><p class="content_header">Profit/Loss</p><p class="amount_header">Rs. <span id="profit_or_loss">-</span></p></div>



                    {{--<div class="col-lg-3 col-md-3"><p class="content_header">Absolute Returns</p><p class="amount_header"><span id="absolute_ret_avg">-</span>%</p></div>--}}

                </div>

                <div class="col-lg-12 col-md-12 padding-lr-zero">
                    <div class="table-wrapper" id="table-wrapper">
                        <h4 class="table-name-header purchase-header">PMS Investment Details</h4>
                        <table class="table table-bordered" id="investment_table">
                            <thead id="inv_table_head">
                            <tr>
                                <th><p class="table_heading">Scheme Name</p></th>
                                <th><p class="table_heading">Corportation Name</p></th>
                                <th><p class="table_heading">Investment Date</p></th>
                                <th><p class="table_heading">Capital Invested</p></th>
                                {{--<th><p class="table_heading">Cost of Investment</p></th>--}}
                                {{--<th><p class="table_heading">Net Amount Invested</p></th>--}}
                                <th><p class="table_heading">Current Value</p></th>
                                <th><p class="table_heading">Total Gain</p></th>
                                <th><p class="table_heading">Abs. Return</p></th>
                                <th><p class="table_heading">Ann. Return</p></th>

                            </tr>
                            </thead>
                            <tbody id="pms_table_body">


                            </tbody>
                        </table>

                    </div>
                </div>


            </div>
        </div>
    </div>


</div>


<!-- Modals Begin below -->


<div id="addSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add Scheme</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_scheme_form">
                    <div class="form-group">
                        <select class="mont-reg" name="scheme_name" id = "scheme_name" required>
                            <option value="106212">SBI Ultra short term Debt fund</option>
                            <option value="112423">l&T Ultra short term fund </option>
                            <option value="114239">ICICI Prudential regular savings fund regular</option>
                            <option value="128053">HDFC Corporate Debt Oppurtunities fund</option>
                            <option value="111803">Birla Sunlife Medium term plan</option>
                            <option value="133805">Kotak Low Duration fund regular plan</option>
                            <option value="133926">DSP Ultra Short Term Fund</option>

                            <option value="104685">ICICI Prudential balanced Advantage fund</option>
                            <option value="100122">HDFC Balanced Fund</option>
                            <option value="118191">L&T India Prudence fund</option>
                            <option value="131666">Birla Sunlife Balanced Advantage Fund</option>
                            <option value="100081">DSP Blackrock Balanced fund</option>

                            <option value="100520">Franklin India Prima Plus Growth</option>
                            <option value="118102">L&T India Value fund</option>
                            <option value="100349">ICICI Prudential Top 100 Fund</option>
                            <option value="112090">Kotak Select Focus fund</option>
                            <option value="105758">HDFC MidCap oppurtunities fund</option>
                            <option value="103360">Franklin India Smaller Companies</option>
                            <option value="113177">Reliance Small Cap fund</option>
                            <option value="101672">Tata Equity P/E fund</option>
                            <option value="104481">DSP Blackrock small and midcap fund</option>

                            <option value="107745">Birla Sunlife Relief 96</option>
                            <option value="103196">Reliance Tax Saver</option>
                            <option value="101979">HDFC Tax Saver</option>

                            <option value="100499">Franklin India Dynamic Accrual Fund </option>
                            <option value="101350">ICICI Prudential Long term Plan</option>
                            <option value="101862">Reliance Banking Fund</option>
                            <option value="102142">Sundaram Rural India Fund</option>
                            <option value="103174">Birla Sun Life Frontline Equity </option>
                            <option value="103504">SBI Blue Chip Fund </option>
                            <option value="106235">Reliance Top 200 Fund </option>
                            <option value="112092">Franklin Build India Fund </option>
                            <option value="112496">L&T Midcap Fund </option>
                            <option value="112600">L&T Infrastructure Fund </option>
                            <option value="112938">Reliance Regular Savings Fund - Debt Option</option>
                            <option value="117716">Kotak Income Opportunities Fund </option>
                            <option value="123690">Kotak Banking & PSU Debt Fund </option>
                            <option value="101537">Sundaram Select Focus</option>
                            <option value="112096">ICICI Prudential long term Growth</option>
                            <option value="107249">Franklin India Ultra-short Bond Fund - Super Institutional - Growth</option>



                        </select>

                    </div>

                    <!--           <div class="form-group">
                                <select class="mont-reg" name="scheme_type" id = "scheme_type" required>
                                    <option value="debt">Debt</option>
                                    <option value="balanced">Balanced</option>
                                    <option value="equity">Equity</option>
                                    <option value="taxsaver">Tax Saver</option>
                                </select>
                              </div> -->

                    <input type="hidden" name="investor_id" id="investor_id" value="">
                    <input type="hidden" name="investor_type" id="investor_type" value="">

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="dop" id = "dop" required placeholder="Enter Date of Purchase">
                    </div>

                    <div class="form-group">
                        <input type="number" class="mont-reg" name="amt_inv" id = "amt_inv" required placeholder="Amount Invested">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="purchase_nav" id = "purchase_nav" required placeholder="Purchase Nav">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="folio_number" id = "folio_number" required placeholder="Folio Number">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Add">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>



<div id="accessModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Access Controller</h4>
            </div>
            <div class="modal-body">
                <!-- <form method="#" action="#" id="access_form"> -->
                <!-- <div class="form-group"> -->
                @foreach($access_users as $user)
                    <p class="access-user">
                        <span>{{$user->name}}</span>
                        <span>
              @if($user->edit_access == 0)
                                <input class="access_check" type="checkbox" name="{{$user->id}}" value="{{$user->id}}">
                            @else
                                <input class="access_check" type="checkbox" name="{{$user->id}}" value="{{$user->id}}" checked="true">
                            @endif
            </span>
                    </p>
            @endforeach
            <!-- </div> -->
                <!-- <div class="form-group"> -->
                <!-- <input type="button" name="add_scheme_btn" id = "save_access" class="btn btn-primary blue-btn" value="Save"> -->
                <!-- </div> -->

                </form>
            </div>
        </div>

    </div>
</div>

<div id="settingsModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Settings</h4>
                <p class="text-center" id="change_pass">Change Password</p>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="change_password_form">
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="old_password" id = "old_password" required placeholder="Old Password">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="new_password" id = "new_password" required placeholder="New password">
                    </div>
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="repeat_password" id = "repeat_password" required placeholder="Repeat New Password">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_scheme_btn" id = "add_scheme" class="btn btn-primary blue-btn" value="Save">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>



<div id="addUserModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add User</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_user_form">
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="new_user_name" id = "new_user_name" required placeholder="User Name">
                    </div>

                    <div class="form-group">
                        <input type="email" class="mont-reg" name="email" id = "email" required placeholder="Enter email">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="password" id = "password" required placeholder="Enter Password">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_user_btn" id = "add_user_btn" class="btn btn-primary blue-btn center-block" value="Add User">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>


<div id="addNewSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add New Scheme</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="add_new_scheme_form" enctype="multipart/form-data">
                    <div class="form-group">
                        <input type="text" class="mont-reg" name="new_scheme_name" id = "new_scheme_name" required placeholder="Scheme Name">
                    </div>

                    <div class="form-group">
                        <input type="text" class="mont-reg" name="scheme_code" id = "scheme_code" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="scheme_type" id = "scheme_type" required>
                            @foreach($scheme_types as $schemes)
                                <option value="{{$schemes['id']}}">{{$schemes['scheme_sub']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <select class="mont-reg" name="amc_name" id = "amc_name" required>
                            @foreach($amc_names as $amc)
                                <option value="{{$amc['id']}}">{{$amc['name']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="file" class="mont-reg" name="scheme_file" id = "scheme_file" required placeholder="Scheme Code">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_user_btn" id = "add_user_btn" class="btn btn-primary blue-btn center-block" value="Add Scheme">
                    </div>

                </form>
            </div>
        </div>

    </div>
</div>


<div id="userStatusModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Status</h4>
            </div>
            <div class="modal-body">
                <p id="addition_status" class="mont-reg text-center"></p>
            </div>
        </div>

    </div>
</div>


<div id="setDateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Set Date</h4>
            </div>
            <div class="modal-body">
                <input type="text" name="nav_date" id="nav_date" class="mont-reg text-center">
            </div>
        </div>

    </div>
</div>


<div id="adminPasswordModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Admin Password</h4>
                <p class="text-center" id="admin_text">Admin Password is required to delete this investment.</p>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <input type="password" class="mont-reg" name="admin_password" id = "admin_password" required placeholder="Enter Admin Password">
                </div>

                <div class="form-group">
                    <input type="submit" name="add_scheme_btn" id = "check_admin_pass" class="btn btn-primary blue-btn" value="Delete">
                </div>


            </div>
        </div>

    </div>
</div>


<div id="addPmsCorpModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add PMS Corporation</h4>
            </div>
            <div class="modal-body">

                <form action="#" id="pmsCorpForm" name="pmsCorpForm">
                    <div class="form-group">
                        <input type=text class="mont-reg input-field" name="pms-corp-name" id = "pms-corp-name" required placeholder="Enter PMS Corporation Name">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_corp_btn" id="add_corp_btn" class="btn btn-primary blue-btn" value="Add">
                    </div>
                </form>


            </div>
        </div>

    </div>
</div>


<div id="addPmsSchemeModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Add PMS Corporation</h4>
            </div>
            <div class="modal-body">

                <form action="#" id="pmsSchemeForm" name="pmsSchemeForm">
                    <div class="form-group">
                        <input type=text class="mont-reg input-field" name="pms-scheme-name" id = "pms-scheme-name" required placeholder="Enter PMS Scheme Name">
                    </div>

                    <div class="form-group">
                        <select name="pms-corp-name" id="pms-corp-name" class="input-field">
                            @foreach($pms_corp as $pms)
                                <option value="{{$pms->id}}">{{$pms->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="add_corp_btn" id="add_corp_btn" class="btn btn-primary blue-btn" value="Add">
                    </div>
                </form>


            </div>
        </div>

    </div>
</div>


<div id="accStatementModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="modal_header">Account Statement Upload</h4>
            </div>
            <div class="modal-body">
                <form method="#" action="#" id="account_statement_form">

                    <div class="form-group">
                        <input type="file" class="mont-reg center-block" name="acc-statement" id = "acc-statement" required>
                        <input type="hidden" name="inv-id" id="inv-id">
                        <input type="hidden" name="upload-type" id="upload-type">
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary center-block modal-submit-btn" value="Upload">
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>



<script type="text/javascript">

    {{--$(document).ready(function(){--}}
    {{--var x={{$aum}};--}}
    {{--x=x.toString();--}}
    {{--var afterPoint = '';--}}
    {{--if(x.indexOf('.') > 0)--}}
    {{--afterPoint = x.substring(x.indexOf('.'),x.length);--}}
    {{--x = Math.floor(x);--}}
    {{--x=x.toString();--}}
    {{--var lastThree = x.substring(x.length-3);--}}
    {{--var otherNumbers = x.substring(0,x.length-3);--}}
    {{--if(otherNumbers != '')--}}
    {{--lastThree = ',' + lastThree;--}}
    {{--var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;--}}

    {{--$('#aum').text(res);--}}
    {{--});--}}

</script>


</body>
</html>

