$(document).ready(function(){


    $(document).on('click','#sidebar_icon',function(){

        var img_src = $('#sidebar_icon>img').attr('src');
        if (img_src == "icons/sidebar.png") {
            $('#sidebar_icon>img').attr('src','icons/sidebar_closed.png');
            $('#sidebar_wrapper').toggle(250,function(){
                $('#contentbar_wrapper').removeClass('col-lg-9 col-md-9 col-sm-9');
                $('#contentbar_wrapper').addClass('col-lg-12 col-md-12 col-sm-12');
            });
        }

        if (img_src == "icons/sidebar_closed.png") {
            $('#sidebar_icon>img').attr('src','icons/sidebar.png');
            $('#sidebar_wrapper').toggle(250,function(){
                $('#contentbar_wrapper').removeClass('col-lg-12 col-md-12 col-sm-12');
                $('#contentbar_wrapper').addClass('col-lg-9 col-md-9 col-sm-9');
            });
        }


    });



    $(document).on('click','.sub-person',function(){

        var group_id = $(this).data('groupid');
        var group_name = $(document).find('a[data-groupname='+group_id+']').text();
        console.log(group_name);

        $('#client_det').empty().append('<span id="client_parent">'+group_name+'</span><span><img src="icons/arrow.png" id="arrow_img" /></span><span id="client_child">'+$(this).text()+'</span>')

        $('#client_parent').text(group_name);
        $('#client_child').text($(this).text());
    });



$('.investor-name').on('click',function(){
    // console.log($(this).data('type'), $(this).data('id'))
    $('#client_det').empty().append('<span id="client_parent">'+$(this).text()+'</span>');


    $('#add_bond_btn').data('invid',$(this).data('id'));
    $('#add_bond_btn').data('invtype',$(this).data('type'));

    $('#ex_investor_id').val($(this).data('id'));
    $('#ex_investor_type').val($(this).data('type'));


    var formData = 'investor_id=' +$(this).data('id')+'&investor_type='+$(this).data('type');
    console.log(formData);

    $.ajax({
        type: "POST",
        url: "/get_bond_investment",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //async : false,
        data: formData,
        cache: false,
        processData: false,
        success: function(data) {
            if(data.msg == true){
                insertBondInterest(data.interest)
                insertBondInvestment(data.investments)
                insertOverallDetails(data.total_amount, data.total_interest_paid, data.total_interest_accumalated);
            }else{

            }
        },
        error: function(xhr, status, error) {

        },
    });


});


$('#add_bond_btn').on('click',function(){

    var inv_id = $(this).data('invid');
    var inv_type = $(this).data('invtype');

    $('#addBondModal').modal('show');


    console.log(inv_id, inv_type);

    $('#add_bond_form').find('#investor_id').attr('value',inv_id);
    $('#add_bond_form').find('#investor_type').attr('value', inv_type);


});

$(document).on('submit', '#add_bond_form', function(e) {

    e.preventDefault();

    var formData = $(this).serialize();
    console.log(formData);


    $.ajax({
        type: "POST",
        url: "/add_bond_investment",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //async : false,
        data: formData,
        cache: false,
        processData: false,
        success: function(data) {

            if(data.status == true) {
                $('#add_bond_form').trigger('reset');
                console.log(data.investment);
                insertBondInvestment(data.investment);
                insertOverallDetails(data.total_amount, data.total_interest_paid, data.total_interest_accumalated);

                $('#addBondModal').modal('hide');
            }else{
                alert(data.error);
            }
        },
        error: function(xhr, status, error) {

        },
    });
});


function insertBondInvestment(data){

    $('#bond_table_body').empty();
        $.each(data, function(key, object){
            var insert = '<tr>'+

                '<td class="bond_name">' +
                    '<div class = "col-xs-8">' +
                        '<p class="table_data scheme_name_p"><span>'+object.name+'</span><a class="add-interest" data-bondid ='+object.id+'>Add Interest</a></p>' +
                        '<div class="download-div">' +
                            '<a><i class="material-icons upload_as" data-dtype="bond" data-placement="bottom" data-invid="'+object.id+'">file_upload</i></a>'+
                            '<a target="_blank" href="/account_statement_download/'+object.id+'/bond"><i class="material-icons download_as" data-placement="bottom" data-invid="'+object.id+'">file_download</i></a>'+
                        '</div>'+
                    '</div>' +
                '<div class="col-xs-4">'+
                '<i class="material-icons delete_bond_investment" data-placement = "bottom" data-toggle = "tooltip" title = "Delete Investment" data-investor_id = "'+object.bondinvestor_id+''+
                '" data-invid = "'+object.id+'" data-invtype = "'+object.bondinvestor_type+'">delete</i>' +
                '<i class="material-icons edit_investment" data-placement="bottom" data-invid="'+object.id+'">edit</i>'+
                // '<a><i class="material-icons upload_as" data-dtype="bond" data-placement="bottom" data-invid="'+object.id+'">file_upload</i></a>'+
                // '<a target="_blank" href="account_statement_download/'+object.id+'/bond"><i class="material-icons download_as" data-placement="bottom" data-invid="'+object.id+'">file_download</i></a>'+
                '</div>'+
                '</td>'+
                '<td><p class="table_data">'+object.amount_invested+'</p></td>'+
                '<td class="dop"><p class="table_data">'+object.date_of_issue+'</p></td>'+
                '<td class="dop"><p class="table_data">'+object.bond_term+'</p></td>'+
                '<td><p class="table_data">'+object.bond_payout_type+'</p></td>'+
                '<td><p class="table_data">'+object.interest_frequency+'</p></td>'+
                '<td><p class="table_data">'+object.interest_rate+'</p></td>'+
                '<td><p class="table_data">'+object.interest_accumalated+'</p></td>'+
                '<td><p class="table_data">'+object.total_interest_paid+'</p></td>'+
                '<td><p class="table_data">'+object.bond_receipt+'</p></td>'+
                '<td><p class="table_data">'+object.credit_rating+'</p></td>'+
                '</tr>';

            $('#bond_table_body').append(insert);
        });
}

$('#doi, #date-of-interest, #due-date, #interest-payout-date').datepicker();

$(document).on('click','.add-interest', function(e){

    $('#bondInterestModal').find('#bond-id').attr('value', $(this).attr('data-bondid'));
    $('#bondInterestModal').modal('show');
});

$('#add_bond_interest_form').on('submit', function(e){
    e.preventDefault();

    var data = $(this).serialize();
    console.log(data);


    $.ajax({
        type: "POST",
        url: "/add_bond_interest",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //async : false,
        data: data,
        cache: false,
        processData: false,
        success: function(data) {

            if(data.status == true) {

                insertBondInterest(data.interest)

                insertOverallDetails(data.total_amount, data.total_interest_paid, data.total_interest_accumalated);

            }else{alert(data.error);
            }
        },
        error: function(xhr, status, error) {

        },
    });
});


function insertBondInterest(interest){

    $('#interest_table_body').empty();

    $.each(interest, function(key, object){
        var insert = '<tr>'+

            '<td class="bond_name">' +
            '<div class = "col-xs-8">' +
            '<p class="table_data scheme_name_p"><span>'+object.bond_name+'</span></p>' +
            '</div>' +
            '<div class="col-xs-4">'+
            '<i class="material-icons delete_bond_interest" data-placement = "bottom" data-toggle = "tooltip" title = "Delete Interest" data-id = "'+object.id+'">delete</i>' +
            // '<i class="material-icons edit_bond_interest" data-placement="bottom" data-invid="\'+object.id+\'">edit</i>'+
            '</div>'+
            '</td>'+
            '<td><p class="table_data">'+object.investment_amount+'</p></td>'+
            '<td class="dop"><p class="table_data">'+object.date_of_issue+'</p></td>'+
            '<td class="dop"><p class="table_data">'+object.interest_frequency+'</p></td>'+
            '<td><p class="table_data">'+object.interest_rate+'</p></td>'+
            // '<td><p class="table_data">'+object.interest_accumalated+'</p></td>'+
            '<td><p class="table_data">'+object.interest_paid+'</p></td>'+
            '<td><p class="table_data">'+object.bond_receipt+'</p></td>'+
            '<td><p class="table_data">'+object.interest_payout_date+'</p></td>'+
            '</tr>';

        $('#interest_table_body').append(insert);
    });

}

function insertOverallDetails(total_amount, interest_paid, interest_accumalated){
    $('#total_amount_invested').text(total_amount);
    $('#total_interest_paid').text(interest_paid);
    $('#total_interest_accumalated').text(interest_accumalated);
}


$(document).on('click', '.delete_bond_interest' ,function(){
    var data = 'id='+$(this).attr('data-id');

    $.ajax({
        type: "POST",
        url: "/delete_bond_interest",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //async : false,
        data: data,
        cache: false,
        processData: false,
        success: function(data) {

            console.log(data);

            if(data.msg == true){
                alert("Bond Interest Deleted Successfully. Kindly Reload and check");
            }else{
                alert("Deletion Failed. Kindly Reload and redo.");
            }
        },
        error: function(xhr, status, error) {

        },
    });

})



$(document).on('click', '.delete_bond_investment' ,function(){
    var data = 'id='+$(this).attr('data-invid');

    $.ajax({
        type: "POST",
        url: "/delete_bond_investment",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        //async : false,
        data: data,
        cache: false,
        processData: false,
        success: function(data) {

            console.log(data);

            if(data.msg == true){
                alert("Bond Investment deleted Successfully. Kindly Reload and check");
            }else{
                alert("Deletion Failed. Kindly Reload and redo.");
            }
        },
        error: function(xhr, status, error) {

        },
    });

})


    $(document).on('click', '.upload_as', function(e){
        e.preventDefault();

        // alert('hello');
        var inv_id = $(this).attr('data-invid');
        var doc_type = $(this).attr('data-dtype');

        $('#account_statement_form').find('#inv-id').val(inv_id);
        $('#account_statement_form').find('#upload-type').val(doc_type);

        console.log(inv_id);
        $('#accStatementModal').modal('show');
    });


    $('#account_statement_form').on('submit', function(e){

        var inv_id = $(this).find('#inv-id').val();
        var upload_type = $(this).find('#upload-type').val();
        e.preventDefault()

        var file = $('#acc-statement').get(0).files[0];
        var ext = $('#acc-statement').val().split('.').pop();


        if(ext != 'pdf'){
            alert('Wrong File Format. PDF file format is expected');
        }else{

            var formData = new FormData();
            formData.append('file',file);
            formData.append('inv-id',inv_id);
            formData.append('upload-type', upload_type);

            $.ajax({
                type: 'POST',
                url: '/account_statement_upload',
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                //async : false,
                contentType: false,
                processData: false,
                success:function(data){
                    if(data.msg == true){
                        alert(data.response);
                        $('#accStatementModal').modal('hide');

                    }else{
                        alert(data.response);

                    }
                },
                error:function(){

                }
            });

        }


    });

});