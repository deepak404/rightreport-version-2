<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'person';


    public function bondInvestment(){
        return $this->morphMany('App\BondDetails', 'bondInvestor');
//        return $this->morphMany('App\BondDetails');

    }

    public function pmsInvestment(){

        return $this->morphMany('App\PmsDetails', 'pmsInvestor');

    }


}
