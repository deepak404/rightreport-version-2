<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmsCorporation extends Model
{

    protected $table = 'pms_corporation';

    protected $fillable = ['name'];

    public function pmsSchemes(){
        return $this->hasMany('App\PmsSchemes', 'corporation_id');
    }

}
