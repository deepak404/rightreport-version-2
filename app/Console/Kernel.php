<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
           Commands\GetNavValues::class,
           Commands\IndexUpdates::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('get:nav')
                 ->daily(00.10)
                 ->timezone('Asia/Kolkata');

        $schedule->command('get:index')
                 ->daily()
                 ->timezone('Asia/Kolkata');
                 
        //$schedule->command('get:nav')->everyMinute();
    }
}
