<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('home');
});*/

Route::get('/', function () {
	$user = Auth::user();
    if (!$user) {
    	return view('login');
    } else {
    	if($user->role == 1) {
    		return redirect('/admin_home');
    	} else {
    		return redirect('/home');
    	}
    }
});

Route::group(['middleware' => ['auth','user']], function () {

	Route::get('/home', 'UserController@index');



	Route::post('/add_group',[
		'uses' => 'UserController@addGroup',
	]);

	Route::post('/remove_group',[
		'uses' => 'UserController@removeGroup',
	]);

	Route::post('/add_person',[
		'uses' => 'UserController@addPerson',
	]);

	Route::post('/get_person',[
		'uses' => 'UserController@getPerson',
	]);

	Route::post('/remove_person',[
		'uses' => 'UserController@removeperson',
	]);


	Route::post('/add_investment',[
		'uses' => 'UserController@addInvestment',
	]);



	Route::get('/get_nav',[
		'uses' => 'UserController@GetNav',
	]);


    Route::get('/update_amc',[
        'uses' => 'UserController@updateAmcNames',
    ]);



    Route::post('/add_sub_person',[

		'uses' => 'UserController@addSubPerson'

		]);

	Route::post('/remove_sub_person',[

		'uses' => 'UserController@removeSubPerson'

		]);



	Route::post('/rename',[

		'uses' => 'UserController@rename'

		]);

	Route::post('/delete_investment',[

		'uses' => 'UserController@deleteInvestment'

		]);


	Route::post('/add_pan',[

		'uses' => 'UserController@addPan'

		]);

	Route::post('/get_query',[

		'uses' => 'UserController@getQuery'

	]);

	Route::post('/add_query',[

		'uses' => 'UserController@addQuery'

	]);

	Route::post('/update_query',[

		'uses' => 'UserController@updateQuery'

	]);


	Route::post('/delete_query',[

		'uses' => 'UserController@deleteQuery'

	]);

	Route::post('/get_investment',[
		'uses' => 'UserController@getInvestment',
	]);

	Route::post('/download_investments',[

		'uses' => 'UserController@downloadInvestments'

	]);

    Route::post('/download_brokerage',[

        'uses' => 'UserController@downloadBrokerage'

    ]);



	Route::post('/cis',[

		'uses' => 'UserController@cis'

	]);

	Route::post('/check_pass',[

		'uses' => 'UserController@checkPass'

	]);

	// Route::get('/portfolio',[

	// 	'uses' => 'UserController@portfolio'

	// ]);

	Route::get('/cispdf',[

		'uses' => 'UserController@cispdf'

	]);

	// Route::get('/id/{id}',[

	// 	'uses' => 'UserController@portfolioBygroup'

	// ]);

	Route::get('/index_update',[

		'uses' => 'UserController@indexupdate'
	]);

	// Route::get('/getAmc',[

	// 	'uses' => 'UserController@getAmc'
	// ]);

	Route::get('/getCSV',[

		'uses' => 'UserController@csvUpdate'	
	]);

	// Route::post('/getAmcdetail',[

	// 	'uses' => 'UserController@amcDetails'
	// ]);

	// Route::get('/bmSingle/{id}',[

	// 	'uses' => 'UserController@benchmark'
	// ]);

	// Route::get('/person/{id}',[

	// 	'uses' => 'UserController@portfolioByperson'
	// ]);

	// Route::get('/bmGroupmember/{id}',[

	// 	'uses' => 'UserController@bmGroup'
	// ]);

	// Route::get('/bmCIS/{id}',[

	// 	'uses' => 'UserController@bmCIS'
	// ]);

	Route::post('/invUpdate',[
		'uses' => 'UserController@invUpdate'
	]);

	Route::get('/div_change',[
		'uses' => 'UserController@switchAcc'
	]);

	Route::post('/update_aum','UserController@updateAum');


    Route::post('/redeem_amount',[
        'uses' => 'UserController@redeemAmount'
    ]);

    Route::post('/delete_redemption',[
        'uses' => 'UserController@deleteRedemption'
    ]);



    Route::post('/show_sip_list',[
        'uses' => 'UserController@showSipList',
    ]);

    Route::post('/edit_investment',[
        'uses' => 'UserController@editInvestment',
    ]);


    Route::get('/bonds',[
        'uses' => 'UserController@showBondsPage'
    ]);


    Route::post('/get_bond_investment',[
        'uses' => 'UserController@getBondInvestment'
    ]);


    Route::post('/add_bond_investment',[
        'uses' => 'UserController@addBondInvestment'
    ]);

    Route::post('/add_bond_interest',[
        'uses' => 'UserController@addBondInterest'
    ]);

    Route::post('/delete_bond_interest',[
        'uses' => 'UserController@deleteBondInterest'
    ]);

    Route::post('/delete_bond_investment',[
        'uses' => 'UserController@deleteBondInvestment'
    ]);

    Route::post('/download_bond_investments',[
        'uses' => 'UserController@downloadBondInvestment'
    ]);


//    PMS starts

    Route::get('/pms',[
        'uses' => 'UserController@showPmsPage'
    ]);


    Route::post('/add_pms_investment',[
        'uses' => 'UserController@addPmsInvestment'
    ]);


    Route::post('/get_pms_investment',[
        'uses' => 'UserController@getPmsInvestment'
    ]);

    Route::post('/delete_pms_investment',[
        'uses' => 'UserController@deletePmsInvestment'
    ]);


    Route::post('/download_pms_investments',[
        'uses' => 'UserController@downloadPmsInvestment'
    ]);

    Route::post('/edit_pms_investment',[
        'uses' => 'UserController@editPmsInvestment'
    ]);


    Route::post('/account_statement_upload',[
        'uses' => 'UserController@accountStatementUpload'
    ]);

    Route::get('/account_statement_download/{inv_id}/{d_type}',[
        'uses' => 'UserController@accountStatementDownload'
    ]);

    Route::get('/find', 'UserController@findUser');

    Route::post('/add_dividend', 'UserController@addDividend');

    Route::post('/delete_dividend', 'UserController@deleteDividend');

    Route::post('/add-sip',[
        'uses' => 'UserController@addSip'
    ]);

    Route::get('/show_sip/{investor_type}/{investor_id}', 'UserController@showSips');


});





Route::group(['middleware' => ['auth','admin']], function () {

	Route::post('/update_access',[

		'uses' => 'AdminController@updateAccess'

	]);

    Route::get('/findAdmin',[
    	'uses' => 'AdminController@findUser'
	]);

	Route::get('/admin_home',[

	'uses' => 'AdminController@home'

	]);

	Route::post('/add_user',[

		'uses' => 'AdminController@addUser'

		]);

	Route::post('/add_scheme',[

		'uses' => 'AdminController@addScheme'

		]);

	Route::post('/admin_get_investment',[
		'uses' => 'AdminController@getInvestment',
	]);
	
	Route::post('/admin_download_investments',[

		'uses' => 'AdminController@downloadInvestments'

		]);

	Route::post('/admin_cis',[

		'uses' => 'AdminController@cis'

		]);

	Route::post('/admin_check_pass',[

	'uses' => 'AdminController@checkPass'

	]);

	Route::post('/admin_delete_investment',[

	'uses' => 'AdminController@deleteInvestment'

	]);

	Route::get('/admin_getAmc',[

		'uses' => 'AdminController@getAmc'
	]);

	Route::post('/getAmcdetail',[

		'uses' => 'AdminController@amcDetails'
	]);
	
	Route::get('/admin_portfolio',[

		'uses' => 'AdminController@portfolio'

	]);

	Route::get('/id/{id}',[

		'uses' => 'AdminController@portfolioBygroup'

	]);

	Route::get('/person/{id}',[

		'uses' => 'AdminController@portfolioByperson'
	]);

	Route::get('/bmSingle/{id}',[

		'uses' => 'AdminController@benchmark'
	]);

	Route::get('/bmGroupmember/{id}',[

		'uses' => 'AdminController@bmGroup'
	]);

	Route::get('/bmCIS/{id}',[

		'uses' => 'AdminController@bmCIS'
	]);

	Route::get('/sectorpdf',[

		'uses' => 'AdminController@sectorPDF'
	]);

	Route::get('/get_notification',[

        'uses' => 'AdminController@getNotification'
	]);

	Route::get('/tickets',[

		'uses' => 'AdminController@getTickets'
	]);

	Route::get('/tickets/{id}',[

		'uses' => 'AdminController@getTicketsFilter'
	]);

	Route::post('/add_ticket',[

		'uses' => 'AdminController@addTicket'
	]);

	Route::post('/get_ticket_data',[

		'uses' => 'AdminController@getTicketData'
	]);

	Route::post('/edit_ticket',[

		'uses' => 'AdminController@editTicket'
	]);

	Route::post('/change_priority',[

		'uses' => 'AdminController@changePriority'
	]);

	Route::post('/change_date',[

		'uses' => 'AdminController@changeDate'
	]);

	Route::post('/delete_ticket',[

		'uses' => 'AdminController@deleteTicket'
	]);

	Route::get('/export_form','AdminController@exportForm');


    Route::post('/get_user_investment_total',[

        'uses' => 'AdminController@getUserInvestmentTotal'
    ]);


    Route::post('/add_pms_corp',[

        'uses' => 'AdminController@addPmsCorp'
    ]);

    Route::post('/add_pms_scheme',[

        'uses' => 'AdminController@addPmsScheme'
    ]);





    Route::get('/admin_bonds',[
        'uses' => 'AdminController@showBondsPage'
    ]);


    Route::post('/admin_get_bond_investment',[
        'uses' => 'UserController@getBondInvestment'
    ]);

    Route::post('/admin_add_bond_investment',[
        'uses' => 'UserController@addBondInvestment'
    ]);

    Route::post('/admin_add_bond_interest',[
        'uses' => 'UserController@addBondInterest'
    ]);

    Route::post('/admin_delete_bond_interest',[
        'uses' => 'UserController@deleteBondInterest'
    ]);

    Route::post('/admin_delete_bond_investment',[
        'uses' => 'UserController@deleteBondInvestment'
    ]);

    Route::post('/admin_download_bond_investments',[
        'uses' => 'UserController@downloadBondInvestment'
    ]);


//    PMS starts

    Route::get('/admin_pms',[
        'uses' => 'AdminController@showPmsPage'
    ]);


    Route::post('/admin_add_pms_investment',[
        'uses' => 'UserController@addPmsInvestment'
    ]);


    Route::post('/admin_get_pms_investment',[
        'uses' => 'UserController@getPmsInvestment'
    ]);

    Route::post('/admin_delete_pms_investment',[
        'uses' => 'UserController@deletePmsInvestment'
    ]);


    Route::post('/admin_download_pms_investments',[
        'uses' => 'UserController@downloadPmsInvestment'
    ]);

    Route::post('/admin_edit_pms_investment',[
        'uses' => 'UserController@editPmsInvestment'
    ]);


     Route::post('/admin_account_statement_upload',[
         'uses' => 'AdminController@accountStatementUpload'
     ]);

     Route::get('/admin_account_statement_download/{inv_id}/{d_type}',[
         'uses' => 'AdminController@accountStatementDownload'
     ]);

    Route::get('/deactivate-investor/{id}/{type}',[
        'uses' => 'AdminController@deactivateInvestor'
    ]);

});

Route::auth();

Route::get('/scheme_change', 'HomeController@schemeNameUpdate');