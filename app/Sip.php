<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sip extends Model
{
    protected $fillable = [
        'scheme_code',
        'sip_amount',
        'investor_id',
        'investor_type'
    ];
}
