<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentDetails extends Model
{

    protected $fillable = ['trail_fee_percent','upfront_fee_percent', 'acc_statement', 'last_fee_update'];

    public function withdrawDetails(){
        return $this->hasMany('App\WithdrawDetails');
    }
}
