<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmsSchemes extends Model
{

    protected $fillable = ['name'];

    public function pmsCorporation(){
        return $this->belongsTo('App\PmsCorporation', 'corporation_id');
    }
}
